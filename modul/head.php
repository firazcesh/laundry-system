	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Laundry System - Laundry System Member </title>

    <!-- Bootstrap CSS-->
    <link href="asset/css/bootstrap.min.css" rel="stylesheet">
	<!-- Jquery CSS-->
	<link href="asset/js/jqueryui/jquery-ui.min.css" rel="stylesheet">
	<!-- DataTables CSS -->
    <link href="asset/datatables/dataTables.bootstrap.css" rel="stylesheet">
	<!-- DataTables Responsive CSS -->
    <link href="asset/datatables/dataTables.responsive.css" rel="stylesheet">

	<!-- Custom CSS -->
	<link href="asset/css/mycustom.css" rel="stylesheet">