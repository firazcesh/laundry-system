<?php
function writeMsg($tipe){
	if ($tipe=='save.sukses') {
		$MsgClass = "alert-success";
		$Msg = "<strong>Sukses!</strong> Data berhasil disimpan.";	
	} else 
	if ($tipe == 'save.gagal') {
		$MsgClass = "alert-danger";
		$Msg = "<strong>Oops!</strong> Data gagal disimpan!";
	}
	else 
	if ($tipe == 'update.sukses') {
		$MsgClass = "alert-dismissible alert-success";
		$Msg = "<strong>Sukses!</strong> Data berhasil diupdate.";
	}
	else 
	if ($tipe == 'update.gagal') {
		$MsgClass = "alert-danger";
		$Msg = "<strong>Oops!</strong> Data gagal diupdate!";
	}
	else 
	if ($tipe == 'kategori.gagal') {
		$MsgClass = "alert-warning";
		$Msg = "<strong>Oops!</strong> Kategori telah terdaftar!";
	}
	else 
	if ($tipe == 'kodesuplier.gagal') {
		$MsgClass = "alert-warning";
		$Msg = "<strong>Oops!</strong> Kode suplier telah terdaftar!";
	}
	else 
	if ($tipe == 'tambahsuplier.gagal') {
		$MsgClass = "alert-warning";
		$Msg = "<strong>Oops!</strong> Data suplier gagal di simpan!";
	}
	else 
	if ($tipe == 'merek.gagal') {
		$MsgClass = "alert-warning";
		$Msg = "<strong>Oops!</strong> Merek telah terdaftar!";
	}
	else 
	if ($tipe == 'username.gagal') {
		$MsgClass = "alert-warning";
		$Msg = "<strong>Oops!</strong> Username telah terdaftar!";
	}
	else 
	if ($tipe == 'tambahuser.gagal') {
		$MsgClass = "alert-warning";
		$Msg = "<strong>Oops!</strong> Data 'user' gagal di simpan!";
	}
	else 
	if ($tipe == 'pass.tdkcocok') {
		$MsgClass = "alert-warning";
		$Msg = "<strong>Oops!</strong> Password baru tidak sama!";
	}
	else 
	if ($tipe == 'passlama.tdkcocok') {
		$MsgClass = "alert-warning";
		$Msg = "<strong>Oops!</strong> Password lama tidak cocok!";
	}
	else 
	if ($tipe == 'password.sukses') {
		$MsgClass = "alert-dismissible alert-success";
		$Msg = "<strong>Sukses!</strong> Password berhasil diganti.";
	}
	else
	if ($tipe == 'tambahproduk.gagal') {
		$MsgClass = "alert-warning";
		$Msg = "<strong>Oops!</strong> Data 'Produk' gagal di simpan!";
	}
	else 
	if ($tipe == 'kprod.gagal') {
		$MsgClass = "alert-warning";
		$Msg = "<strong>Oops!</strong> Kode Produk telah terdaftar!";
	}
	echo "<div class=\"alert alert-dismissible ".$MsgClass."\">
  	  <button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>
  	  ".$Msg."
	  </div>";		  
}
?>