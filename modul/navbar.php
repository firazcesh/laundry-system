<?php
if ($_SESSION['level'] == 'admin') {
    ?>
	<div class="navbar navbar-default navbar-fixed-top">
		<div class="container">
			<div class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li class="active"><a href="dashboard.php"><span class="glyphicon glyphicon-home"></span> Home</a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-bed"></span> Transaksi Laundry<b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="dashboard.php?p=pembelian_member"><span class="glyphicon glyphicon-bed"></span> Laundry Member</a></li>
								<li><a href="dashboard.php?p=penjualan"><span class="glyphicon glyphicon-bed"></span> Laundry Non Member</a></li>
							</ul>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-shopping-cart"></span> Transaksi Produk<b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="dashboard.php?p=pembelian"><span class="glyphicon glyphicon-shopping-cart"></span> Pembelian Produk</a></li>
								<li><a href="dashboard.php?p=penjualan"><span class="glyphicon glyphicon-shopping-cart"></span> Pemakaian Produk</a></li>
							</ul>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-file"></span> Data Produk<b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="dashboard.php?p=produk"><span class="glyphicon glyphicon-list-alt"></span> Data Produk</a></li>
								<li class="divider"></li>
								<li><a href="dashboard.php?p=kategori_produk"><span class="glyphicon glyphicon-list-alt"></span> Kategori Produk</a></li>
								<li><a href="dashboard.php?p=merek_produk"><span class="glyphicon glyphicon-list-alt"></span> Merek Produk</a></li>
							</ul>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-list-alt"></span> Data Member<b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="dashboard.php?p=member"><span class="glyphicon glyphicon-list-alt"></span> Data Member</a></li>
								<li class="divider"></li>
								<li><a href="dashboard.php?p=kategori_member"><span class="glyphicon glyphicon-list-alt"></span> Kategori Member</a></li>
								<!-- <li><a href="dashboard.php?p=merek_produk"><span class="glyphicon glyphicon-list-alt"></span> Merek Produk</a></li> -->
							</ul>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-print"></span> Laporan<b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li><a href="dashboard.php?p=laporan_pembelian"><span class="glyphicon glyphicon-shopping-cart"></span> Laporan Pembelian Produk</a></li>
							<li><a href="dashboard.php?p=laporan_penjualan"><span class="glyphicon glyphicon-shopping-cart"></span> Laporan Pemakaian Produk</a></li>
						</ul>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-folder-open"></span> Management Data<b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="dashboard.php?p=pelanggan"><span class="glyphicon glyphicon-book"></span> Data Pelanggan</a></li>
								<li><a href="dashboard.php?p=suplier"><span class="glyphicon glyphicon-book"></span> Data Suplier</a></li>
								<li><a href="dashboard.php?p=user"><span class="glyphicon glyphicon-book"></span> Data User</a></li>
								<li><a href="dashboard.php?p=contact"><span class="glyphicon glyphicon-book"></span> Data Perusahaan</a></li>
							</ul>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span> User<b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="dashboard.php?p=ubah_password"><span class="glyphicon glyphicon-lock"></span> Ubah Password</a></li>
								<li class="divider"></li>
								<li><a href="modul/logout.php"><span class="glyphicon glyphicon-off"></span> Logout</a></li>
							</ul>
					</li>
				</ul>
			</div>
		</div>
    </div>
<?php
} elseif ($_SESSION['level'] == 'administrasi') {
        ?>
	<div class="navbar navbar-default navbar-fixed-top">
		<div class="container">
			<div class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li class="active"><a href="dashboard.php"><span class="glyphicon glyphicon-home"></span> Home</a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-shopping-cart"></span> Transaksi<b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="dashboard.php?p=pembelian"><span class="glyphicon glyphicon-shopping-cart"></span> Pembelian</a></li>
								<li><a href="dashboard.php?p=penjualan"><span class="glyphicon glyphicon-shopping-cart"></span> Penjualan</a></li>
							</ul>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-file"></span> Data Produk<b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="dashboard.php?p=produk"><span class="glyphicon glyphicon-list-alt"></span> Data Produk</a></li>
								<li class="divider"></li>
								<li><a href="dashboard.php?p=kategori_produk"><span class="glyphicon glyphicon-list-alt"></span> Kategori Produk</a></li>
								<li><a href="dashboard.php?p=merek_produk"><span class="glyphicon glyphicon-list-alt"></span> Merek Produk</a></li>
							</ul>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-folder-open"></span> Management Data<b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="dashboard.php?p=pelanggan"><span class="glyphicon glyphicon-book"></span> Data Pelanggan</a></li>
								<li><a href="dashboard.php?p=suplier"><span class="glyphicon glyphicon-book"></span> Data Suplier</a></li>
							</ul>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span> User<b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="dashboard.php?p=ubah_password"><span class="glyphicon glyphicon-lock"></span> Ubah Password</a></li>
								<li class="divider"></li>
								<li><a href="modul/logout.php"><span class="glyphicon glyphicon-off"></span> Logout</a></li>
							</ul>
					</li>
				</ul>
			</div>
		</div>
    </div>
<?php
    } elseif ($_SESSION['level'] == 'gudang') {
        ?>
	<div class="navbar navbar-default navbar-fixed-top">
		<div class="container">
			<div class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li class="active"><a href="dashboard.php"><span class="glyphicon glyphicon-home"></span> Home</a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-file"></span> Data Produk<b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="dashboard.php?p=produk"><span class="glyphicon glyphicon-list-alt"></span> Data Produk</a></li>
								<li class="divider"></li>
								<li><a href="dashboard.php?p=kategori_produk"><span class="glyphicon glyphicon-list-alt"></span> Kategori Produk</a></li>
								<li><a href="dashboard.php?p=merek_produk"><span class="glyphicon glyphicon-list-alt"></span> Merek Produk</a></li>
							</ul>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span> User<b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="dashboard.php?p=ubah_password"><span class="glyphicon glyphicon-lock"></span> Ubah Password</a></li>
								<li class="divider"></li>
								<li><a href="modul/logout.php"><span class="glyphicon glyphicon-off"></span> Logout</a></li>
							</ul>
					</li>
				</ul>
			</div>
		</div>
    </div>
<?php
    } elseif ($_SESSION['level'] == 'keuangan') {
        ?>
	<div class="navbar navbar-default navbar-fixed-top">
		<div class="container">
			<div class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li class="active"><a href="dashboard.php"><span class="glyphicon glyphicon-home"></span> Home</a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-print"></span> Laporan<b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li><a href="dashboard.php?p=laporan_pembelian"><span class="glyphicon glyphicon-shopping-cart"></span> Laporan Pembelian</a></li>
							<li><a href="dashboard.php?p=laporan_penjualan"><span class="glyphicon glyphicon-shopping-cart"></span> Laporan Penjualan</a></li>
						</ul>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span> User<b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="dashboard.php?p=ubah_password"><span class="glyphicon glyphicon-lock"></span> Ubah Password</a></li>
								<li class="divider"></li>
								<li><a href="modul/logout.php"><span class="glyphicon glyphicon-off"></span> Logout</a></li>
							</ul>
					</li>
				</ul>
			</div>
		</div>
    </div>


<?php
    }
?>