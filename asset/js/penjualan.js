
		//meload isi tabel
		$("#barang").load("asset/ajax/ajaxpenjualan.php","op=barang");

					
//autocomplete script
$(document).on('focus','.autocomplete_txt',function(){
	type = $(this).data('type');
	
	if(type =='productNama' )autoTypeNo=0;
	if(type =='productKode' )autoTypeNo=1; 	
	
	$(this).autocomplete({
		source: function( request, response ) {
			$.ajax({
				url : 'asset/ajax/ajaxpenjualan.php',
				dataType: "json",
				method: 'post',
				data: {
				   name_startsWith: request.term,
				   type: type
				},
				 success: function( data ) {
					 response( $.map( data, function( item ) {
					 	var code = item.split("|");
						return {
							label: code[autoTypeNo],
							value: code[autoTypeNo],
							data : item
						}
					}));
				}
			});
		},
		autoFocus: true,	      	
		minLength: 0,
		select: function( event, ui ) {
			var names = ui.item.data.split("|");						
			id_arr = $(this).attr('id');
	  		id = id_arr.split("_");
			$('#nama').val(names[0]);
			$('#kode').val(names[1]);
			$('#idprod').val(names[2]);
			$('#harga').val(names[3]);
			$('#stok').val(names[4]);
		}		      	
	});
});

//mendeksripsikan variabel yang akan digunakan
    var nota;
	var tanggal;
	var idprod;
	var kode;
	var nama;
    var harga;
    var jumlah;
    var stok;
	
    $(function(){
        //jika tombol tambah di klik
        $("#tambah").click(function(){
            nama=$("#nama").val();
            stok=$("#stok").val();
            jumlah=$("#jumlah").val();
	
			if(nama==""){
                alert("Nama Produk Harus diisi");
                exit();
                }else if(jumlah < 1){
					alert("Jumlah jual tidak boleh 0");
					$("#jumlah").focus();
					exit();
					}
				
				idprod=$("#idprod").val();	
                kode=$("#kode").val();
                harga=$("#harga").val();
                        
                $("#status").html("sedang diproses. . .");
                $("#loading").show();
                        
                $.ajax({
                    url:"asset/ajax/ajaxpenjualan.php",
                    data:"op=tambah&idprod="+idprod+"&harga="+harga+"&jumlah="+jumlah,
                    cache:false,
                    success:function(msg){
                    if(msg=="sukses"){
                        $("#status").html("Berhasil disimpan. . .");
						}else{
							$("#status").html("Eror. . .");
							}
                            $("#loading").hide();
                            $("#nama").val("");
                            $("#kode").val("");
                            $("#idprod").val("");
                            $("#harga").val("");
                            $("#stok").val("");
                            $("#jumlah").val("");
                            $("#barang").load("asset/ajax/ajaxpenjualan.php","op=barang");
                    }
                });
            });
			
            //jika tombol proses diklik
            $("#proses").click(function(){
				pelanggan=$("#pelanggan").val();
                kode_jual=$("#kode_jual").val();
                tanggal=$("#tanggal").val();
				catatan=$("#catatan").val();
				pegawai=$("#pegawai").val();
				if(pelanggan==""){
                alert("Pelanggan Harus diisi");
                exit();
                }
				
                $.ajax({
                    url:"asset/ajax/ajaxpenjualan.php",
                    data:"op=proses&pelanggan="+pelanggan+"&kode_jual="+kode_jual+"&tanggal="+tanggal+"&catatan="+catatan+"&pegawai="+pegawai,
                    cache:false,
                    success:function(msg){
                        if(msg=='sukses'){
                            $("#status").html('Transaksi Pembelian berhasil');
                            alert('Transaksi Berhasil');
                            exit();
                        }else{
                            $("#status").html('Transaksi Gagal');
                            alert('Transaksi Gagal');
                            exit();
                            }
                        $("#barang").load("asset/ajax/ajaxpenjualan.php","op=barang");
                        $("#nama").val("");
                        $("#kode").val("");
                        $("#idprod").val("");
                        $("#harga").val("");
                        $("#stok").val("");
                        $("#jumlah").val("");
                        $("#loading").hide();
                    }
                })
            })
        });