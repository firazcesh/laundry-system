//meload isi tabel
$("#barang").load("asset/ajax/ajaxpembelian.php","op=barang");
					
//autocomplete script
$(document).on('focus','.autocomplete_txt',function(){
	type = $(this).data('type');
	
	if(type =='productNama' )autoTypeNo=0;
	if(type =='productKode' )autoTypeNo=1; 	
	
	$(this).autocomplete({
		source: function( request, response ) {
			$.ajax({
				url : 'asset/ajax/ajaxpembelian.php',
				dataType: "json",
				method: 'post',
				data: {
				   name_startsWith: request.term,
				   type: type
				},
				 success: function( data ) {
					 response( $.map( data, function( item ) {
					 	var code = item.split("|");
						return {
							label: code[autoTypeNo],
							value: code[autoTypeNo],
							data : item
						}
					}));
				}
			});
		},
		autoFocus: true,	      	
		minLength: 0,
		select: function( event, ui ) {
			var names = ui.item.data.split("|");						
			id_arr = $(this).attr('id');
	  		id = id_arr.split("_");
			$('#nama').val(names[0]);
			$('#kode').val(names[1]);
			$('#idprd').val(names[2]);
		}		      	
	});
});

//mendeksripsikan variabel yang akan digunakan
    var nota;
	var tanggal;
	var idprd;
	var kode;
	var nama;
    var harga;
    var jumlah;
    var stok;
	
    $(function(){		
        //jika tombol tambah di klik
        $("#tambah").click(function(){
            idprd=$("#idprd").val();
            nama=$("#nama").val();
            jumlah=$("#jumlah").val();
            harga=$("#harga").val();
            if(nama==""){
                alert("Nama Produk Harus diisi");
                exit();
                }else if(harga==""){
                    alert("Harga tidak boleh kosong!");
                    $("#harga").focus();
                    exit();
					}else if(harga < 1){
						alert("Harga beli tidak boleh 0");
						$("#harga").focus();
						exit();
						}else if(jumlah < 1){
							alert("Jumlah beli tidak boleh 0");
							$("#jumlah").focus();
							exit();
							}
                kode=$("#kode").val();
                        
                $("#status").html("sedang diproses. . .");
                $("#loading").show();
                        
                $.ajax({
                    url:"asset/ajax/ajaxpembelian.php",
                    data:"op=tambah&idprd="+idprd+"&harga="+harga+"&jumlah="+jumlah,
                    cache:false,
                    success:function(msg){
                    if(msg=="sukses"){
                        $("#status").html("Berhasil disimpan. . .");
						}else{
							$("#status").html("ERROR. . .");
							}
                            $("#loading").hide();
                            $("#nama").val("");
                            $("#harga").val("");
                            $("#jumlah").val("");
                            $("#kode").val("");
                            $("#idprd").val("");
                            $("#barang").load("asset/ajax/ajaxpembelian.php","op=barang");
                    }
                });
            });
			
            //jika tombol proses diklik
            $("#proses").click(function(){
				kodebeli=$("#kodebeli").val();
                nota=$("#nota").val();
                tanggal=$("#tanggal").val();
				suplier=$("#suplier").val();
				keterangan=$("#keterangan").val();
				pegawai=$("#pegawai").val();
                if(tanggal==""){
                alert("Tanggal Harus diisi");
                exit();
                }else if(suplier==""){
                    alert("Suplier tidak boleh kosong!");
                    $("#suplier").focus();
                    exit();
                }else if(nota==""){
                    alert("Nota beli tidak boleh kosong");
                    $("#nota").focus();
                    exit();
                    }
                $.ajax({
                    url:"asset/ajax/ajaxpembelian.php",
                    data:"op=proses&nota="+nota+"&tanggal="+tanggal+"&kodebeli="+kodebeli+"&suplier="+suplier+"&keterangan="+keterangan+"&pegawai="+pegawai,
                    cache:false,
                    success:function(msg){
                        if(msg=='sukses'){
                            $("#status").html('Transaksi Pembelian berhasil');
                            alert('Transaksi Berhasil');
                            exit();
                        }else{
                            $("#status").html('Transaksi Gagal');
                            alert('Transaksi Gagal');
                            exit();
                            }
                        $("#nama").val("");
                        $("#kode").val("");
                        $("#idprd").val("");
                        $("#harga").val("");
                        $("#jumlah").val("");
                        $("#barang").load("asset/ajax/ajaxpembelian.php","op=barang");
                        $("#loading").hide();
                    }
                })
            })
        });