-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 14, 2019 at 04:32 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 5.6.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lound`
--

-- --------------------------------------------------------

--
-- Table structure for table `belisementara`
--

CREATE TABLE `belisementara` (
  `idprod` int(8) NOT NULL,
  `harga` int(15) NOT NULL,
  `jumlah` int(8) NOT NULL,
  `subtotal` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id` int(5) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `keterangan` varchar(150) NOT NULL,
  `pemilik` varchar(50) NOT NULL,
  `no_tlp` varchar(14) NOT NULL,
  `email` varchar(100) NOT NULL,
  `alamat` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `nama`, `keterangan`, `pemilik`, `no_tlp`, `email`, `alamat`) VALUES
(1, 'PT. Iraz Gitz', 'Agen Retail Koperasi Sagara', 'Faisal Firaz', '082258182621', 'faisalfiraz@gmail.com', 'Jalan dewi sartika XI blok G 74 Perumahan Pejuang Jaya Kota Bekasi Barat');

-- --------------------------------------------------------

--
-- Table structure for table `detail_pembelian`
--

CREATE TABLE `detail_pembelian` (
  `id` int(11) NOT NULL,
  `kode_beli` varchar(8) NOT NULL,
  `produk_id` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `harga` int(15) NOT NULL,
  `subtotal` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `detail_penjualan`
--

CREATE TABLE `detail_penjualan` (
  `id` int(11) NOT NULL,
  `kode_jual` varchar(8) NOT NULL,
  `produk_id` int(11) NOT NULL,
  `harga` int(15) NOT NULL,
  `jumlah` int(8) NOT NULL,
  `subtotal` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jualsementara`
--

CREATE TABLE `jualsementara` (
  `idprod` int(8) NOT NULL,
  `harga` int(15) NOT NULL,
  `jumlah` int(8) NOT NULL,
  `subtotal` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kategori_member`
--

CREATE TABLE `kategori_member` (
  `id` int(11) NOT NULL,
  `kategori` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori_member`
--

INSERT INTO `kategori_member` (`id`, `kategori`) VALUES
(35, 'Perumahan'),
(37, 'Apartemant');

-- --------------------------------------------------------

--
-- Table structure for table `kategori_produk`
--

CREATE TABLE `kategori_produk` (
  `id` int(11) NOT NULL,
  `kategori` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE `member` (
  `id` int(11) NOT NULL,
  `kode` varchar(255) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `ktp` varchar(255) DEFAULT NULL,
  `alamat` text NOT NULL,
  `kategori_id` int(11) NOT NULL,
  `merek_id` int(11) NOT NULL,
  `harga_jual` int(15) NOT NULL,
  `stok` int(8) NOT NULL DEFAULT '0',
  `satuan` varchar(15) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `no_telpon` varchar(255) DEFAULT NULL,
  `ket` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `merek_produk`
--

CREATE TABLE `merek_produk` (
  `id` int(11) NOT NULL,
  `merek` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pelanggan`
--

CREATE TABLE `pelanggan` (
  `id` int(11) NOT NULL,
  `kode` varchar(6) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `alamat` varchar(300) NOT NULL,
  `no_tlp` varchar(14) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pelanggan`
--

INSERT INTO `pelanggan` (`id`, `kode`, `nama`, `alamat`, `no_tlp`) VALUES
(0, 'PL0001', 'SAGARA', 'Jl. Mampang Prapatan Raya 108\r\nRukan Buncit Mas, Blok C3A\r\nDuren Tiga, Jakarta 12760', '02179170400');

-- --------------------------------------------------------

--
-- Table structure for table `pembelian`
--

CREATE TABLE `pembelian` (
  `kode_beli` varchar(8) NOT NULL,
  `nota_beli` varchar(50) NOT NULL,
  `suplier` varchar(100) NOT NULL,
  `pegawai` varchar(100) NOT NULL,
  `tanggal_beli` date DEFAULT NULL,
  `total_beli` int(15) NOT NULL,
  `keterangan_beli` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `penjualan`
--

CREATE TABLE `penjualan` (
  `kode_jual` varchar(8) NOT NULL,
  `tanggal_jual` date DEFAULT NULL,
  `pelanggan` varchar(100) NOT NULL,
  `total_jual` varchar(15) NOT NULL,
  `catatan_jual` varchar(300) NOT NULL,
  `pegawai` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE `produk` (
  `id` int(11) NOT NULL,
  `kode` varchar(10) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `keterangan` varchar(300) NOT NULL,
  `kategori_id` int(11) NOT NULL,
  `merek_id` int(11) NOT NULL,
  `harga_jual` int(15) NOT NULL,
  `stok` int(8) NOT NULL DEFAULT '0',
  `satuan` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `suplier`
--

CREATE TABLE `suplier` (
  `id` int(5) NOT NULL,
  `kode` varchar(5) DEFAULT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `alamat` varchar(255) NOT NULL,
  `no_tlp` char(14) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(5) NOT NULL,
  `nip` varchar(8) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(100) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `alamat` varchar(300) NOT NULL,
  `jenis_kelamin` varchar(9) NOT NULL,
  `no_tlp` varchar(12) NOT NULL,
  `email` varchar(100) NOT NULL,
  `blokir` varchar(1) NOT NULL,
  `level` enum('administrasi','admin','keuangan','gudang') DEFAULT 'admin'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `nip`, `username`, `password`, `nama`, `alamat`, `jenis_kelamin`, `no_tlp`, `email`, `blokir`, `level`) VALUES
(1, 'PG001111', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'administrator', 'Perum Jatihurip Blok 6 No. 202 RT.01 RW.013 , Desa Jatihurip - Sumedang', 'Laki-laki', '082130046934', 'admin@sembilanmatahari.com', 'N', 'admin'),
(2, 'PG001112', 'administrasi', '5f4dcc3b5aa765d61d8327deb882cf99', 'Bag. Administrasi', 'Sumedang', 'Laki-laki', '08568333883', 'administrasi@sembilanmatahari.com', 'N', 'administrasi'),
(3, 'PG001113', 'gudang', '202446dd1d6028084426867365b0c7a1', 'Bag. Gudang', 'Bandung', 'Laki-laki', '08568333883', 'gudang@sembilanmatahari.com', 'N', 'gudang'),
(4, 'PG001114', 'keuangan', 'e10adc3949ba59abbe56e057f20f883e', 'Bag. Keuangan', 'Sumedang', 'Laki-laki', '08568333883', 'keuangan@sembilanmatahari.com', 'N', 'keuangan');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `detail_pembelian`
--
ALTER TABLE `detail_pembelian`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `detail_penjualan`
--
ALTER TABLE `detail_penjualan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kategori_member`
--
ALTER TABLE `kategori_member`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kategori_produk`
--
ALTER TABLE `kategori_produk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `merek_produk`
--
ALTER TABLE `merek_produk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pembelian`
--
ALTER TABLE `pembelian`
  ADD PRIMARY KEY (`kode_beli`),
  ADD KEY `kode_beli` (`kode_beli`),
  ADD KEY `kode_beli_2` (`kode_beli`);

--
-- Indexes for table `penjualan`
--
ALTER TABLE `penjualan`
  ADD PRIMARY KEY (`kode_jual`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suplier`
--
ALTER TABLE `suplier`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `detail_pembelian`
--
ALTER TABLE `detail_pembelian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `detail_penjualan`
--
ALTER TABLE `detail_penjualan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kategori_member`
--
ALTER TABLE `kategori_member`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `kategori_produk`
--
ALTER TABLE `kategori_produk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `member`
--
ALTER TABLE `member`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `merek_produk`
--
ALTER TABLE `merek_produk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `produk`
--
ALTER TABLE `produk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `suplier`
--
ALTER TABLE `suplier`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
