<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Daftar Produk</h1>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-6">
				<a href="dashboard.php?p=tambah_produk"><button class="btn btn-success"> + Tambah Produk </button></a>
			</div>
			<div class="col-lg-6">
			<?php
                //kode php ini kita gunakan untuk menampilkan pesan eror
                if (!empty($_GET['psn'])) {
                    if ($_GET['psn'] == 1) {
                        writeMsg('kprod.gagal');
                    } elseif ($_GET['psn'] == 2) {
                        writeMsg('save.sukses');
                    } elseif ($_GET['psn'] == 3) {
                        writeMsg('save.gagal');
                    } elseif ($_GET['psn'] == 4) {
                        writeMsg('update.sukses');
                    } elseif ($_GET['psn'] == 5) {
                        writeMsg('update.gagal');
                    }
                }
            ?>
			</div>
		</div>

		<br />
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-info">
                    <div class="panel-heading">
                        Daftar Produk
                    </div>

                    <div class="panel-body">
                        <div class="dataTable_wrapper">
							<table class="table table-striped table-bordered table-paginate">
								<thead>
									<tr>
										<th><center>No.</center></th>
										<th><center>Kode Produk</center></th>
										<th>Nama Produk</th>
										<th><center>Kategori</center></th>
										<th><center>Merek</center></th>
										<th><center>Satuan</center></th>
										<th><center>Harga</center></th>
										<th><center>Stok</center></th>
										<th width="50px"><center>Aksi</center></th>
									</tr>
								</thead>
								<tbody>
								<?php
                                    $jumlah_desimal = '0';
                                    $pemisah_desimal = ',';
                                    $pemisah_ribuan = '.';
                                    $sql = mysql_query('SELECT
															a.id,
															a.kode,
															a.nama,
															b.kategori AS kategori_id,
															c.merek AS merek_id,
															a.harga_jual,
															a.satuan,
															a.stok
															FROM
															produk a, kategori_produk b, merek_produk c
															WHERE
															a.kategori_id=b.id
															AND a.merek_id=c.id
															ORDER BY nama ASC');
                                    $no = 1;
                                    while ($row = mysql_fetch_array($sql)) {
                                        ?>
									<tr>
										<td align="center"><?php echo $no; ?></td>
										<td align="center"><?php echo $row['kode']; ?></td>
										<td><?php echo $row['nama']; ?></td>
										<td align="center"><?php echo $row['kategori_id']; ?></td>
										<td align="center"><?php echo $row['merek_id']; ?></td>
										<td align="center"><?php echo $row['satuan']; ?></td>
										<td align="right"><b><?php echo number_format($row['harga_jual'], $jumlah_desimal, $pemisah_desimal, $pemisah_ribuan).',-'; ?></b></td>
										<td align="center"><?php echo $row['stok']; ?></td>
										<td align="center">
											<a class="btn btn-xs btn-success" href="dashboard.php?p=edit_produk&id=<?php echo $row['id']; ?>"><span class="glyphicon glyphicon-edit"></span></a>

											<a class="btn btn-xs btn-danger" href="pages/aksi/produkhapus-proses.php?id=<?php echo $row['id']; ?>" onclick ="if (!confirm('Apakah Anda yakin akan menghapus data ini?')) return false;"> <span class="glyphicon glyphicon-trash"></span></a>
										</td>
									</tr>
								<?php ++$no;
                                    } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>