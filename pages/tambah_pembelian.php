
	<?php
	// Script membuat kide beli bertambah otomatis
	$sql=mysql_query("SELECT kode_beli from pembelian order by kode_beli DESC LIMIT 0,1");
	$data=mysql_fetch_array($sql);
	$kodeawal=substr($data['kode_beli'],3,5)+1;
		if($kodeawal<10){
			$kode='BLJ0000'.$kodeawal;
			}
			elseif($kodeawal > 9 && $kodeawal <=99){
				$kode='BLJ000'.$kodeawal;
			}else{
				$kode='BLJ000'.$kodeawal;
			}
	?>

	<div class="container">		
		<div class="row">
			<div class="col-lg-12">
				<form class="form-horizontal" action="" method="POST">				
					<div class="form-group">
						<label for="tanggal" class="col-lg-1 control-label"><strong>Tanggal : </strong></label>
						<div class="col-lg-2">
							<input type="date" class="form-control" id="tanggal">
						</div>
						
						<label for="pegawai" class="col-lg-2 col-lg-offset-3 control-label"><strong>Petugas : </strong></label>
						<div class="col-lg-4">
							<input type="text" class="form-control" id="pegawai" value="<?php echo ($_SESSION['nama'])?>" readonly>
						</div>
					</div>
								
					<div class="form-group">
						<label for="kodebeli" class="col-lg-1 control-label"><strong>Kode : </strong></label>
						<div class="col-lg-2">
							<input type="text" maxlength="8" class="form-control uneditable-input" id="kodebeli" value="<?php echo $kode;?>" readonly>
						</div>
								
						<label for="nota" class="col-lg-2 control-label"><strong>Nota : </strong></label>
						<div class="col-lg-2">
							<input type="text" maxlength="30" class="form-control" id="nota" name="nota" placeholder="Nota beli ...">
						</div>
									
						<label for="suplier" class="col-lg-1 control-label"><strong>Suplier : </strong></label>
						<div class="col-lg-4">
							<select class="form-control" id="suplier">
								<option value="">Pilih Suplier</option>
								<?php 
								$query = mysql_query("SELECT * FROM suplier ORDER by nama ASC");
								while($row = mysql_fetch_array($query)){
									echo '<option value="'.$row['id'].'">'.$row['nama'].'</option>';
								} ?>
							</select>
						</div>
					</div>
					
					<div class="form-group">
						<label for="keterangan" class="col-lg-2 col-lg-offset-3 control-label"><strong>Keterangan : </strong></label>
						<div class="col-lg-7">
							<textarea  maxlength="300" rows="2" class="form-control" id="keterangan" placeholder="Keterangan ..."></textarea>
						</div>
					</div>				
					
					<legend>List Produk</legend>
					<div class="form-group">
						<div class="col-lg-4"><input class="form-control autocomplete_txt" data-type="productNama" type='text' id='nama' maxlength="100"placeholder="Nama Produk"></div>
						<div class="col-lg-2"><input class="form-control autocomplete_txt" data-type="productKode" type='text' id='kode' placeholder="Kode Produk" readonly><input class="hidden" type="text" id="idprd"></div>
						<div class="col-lg-2"><div class="input-group"><span class="input-group-addon">Rp</span><input class="form-control" type='number' id='harga' placeholder="Harga" maxlength="15"></div></div>
						<div class="col-lg-1"><input class="form-control" type='text' id='jumlah' maxlength="8" placeholder="Jumlah"></div>
                        <div class="col-lg-1"><button id="tambah" class="btn btn-success">Tambah</button></div>
                        <span id="status"></span>
					</div>
					
					<table id="barang" class='table table-bordered table-hover'>
						
					</table>
					
					<div class="form-actions">
						<div class="col-lg-12">
							<button class="btn btn-success" type="submit" id="proses">Proses</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="asset/js/jquery-1.11.0.js"></script>
	<script src="asset/js/pembelian.js"></script>