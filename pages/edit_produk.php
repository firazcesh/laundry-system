<?php
$sql = mysql_query("SELECT * from produk where id='$_GET[id]'");
$row = mysql_fetch_array($sql);
?>	

	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Edit Data Produk</h1>
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-2">
				<a href="dashboard.php?p=produk"><button class="btn btn-danger"><span class="glyphicon glyphicon-arrow-left"> Kembali</span></button></a>
			</div>
			<div class="col-lg-4">
				<span id="head" class="label label-info"></span>
			</div>
		</div>
		
				<br />
		
		<div class="row">
			<div class="col-lg-12">
				<form class="form-horizontal" method="POST" onsubmit='return formValidation()'>
					<div class="form-group">
						<label for="kode" class="col-lg-2 control-label">Kode Produk : </label>
						<div class="col-lg-2">
							<input type="text" maxlength="10" class="form-control" id="kode" name="kode" value="<?php echo $row['kode']  ;?>" class="uneditable-input" readonly="true">
						</div>
					</div>
					
					<div class="form-group">
						<label for="nama" class="col-lg-2 control-label">Nama Produk : </label>
						<div class="col-lg-6">
							<input type="text" maxlength="100" class="form-control" id="nama" name="nama" value="<?php echo $row['nama']  ;?>" readonly="true">
						</div>
					</div>
					
					<div class="form-group">
						<label for="keterangan" class="col-lg-2 control-label">Keterangan : </label>
						<div class="col-lg-6">
							<textarea  maxlength="300" class="form-control" id="keterangan" name="keterangan"><?php echo $row['keterangan']  ;?></textarea>
						</div>
					</div>
					
					<div class="form-group">
						<label for="harga_jual" class="col-lg-2 control-label">Harga Jual: </label>
						<div class="col-lg-6">
							<input type="text" maxlength="15" class="form-control" id="harga_jual" name="harga_jual" value="<?php echo $row['harga_jual']  ;?>" required>
						</div>
						<span id="h1" class="label label-danger"></span>
					</div>
					
					<div class="form-group">
						<label for="stok" class="col-lg-2 control-label">Stok : </label>
						<div class="col-lg-6">
							<input type="text" maxlength="8" class="form-control" id="stok" name="stok" value="<?php echo $row['stok']  ;?>" readonly="true">
						</div>
					</div>
					
					<div class="form-group">
						<label for="satuan" class="col-lg-2 control-label">Satuan : </label>
						<div class="col-lg-6">
							<input type="text" maxlength="15" class="form-control" id="satuan" name="satuan" value="<?php echo $row['satuan']  ;?>" readonly="true">
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-lg-10 col-lg-offset-2">
							<button class="btn btn-success" type="submit" value="Update" name="update">Update</button>
						</div>
					</div>
				<?php
				if (isset($_POST["update"])) {
					mysql_query("UPDATE produk SET harga_jual = '".$_POST['harga_jual']."', keterangan = '".$_POST['keterangan']."' WHERE id = '".$_GET['id']."'");

					//Re-Load Data from DB
					$sql = mysql_query("SELECT keterangan, harga_jual FROM produk WHERE id = '".$_GET['id']."'");
					$row = mysql_fetch_array($sql);
					
					header('location: dashboard.php?p=produk&psn=4');
				}
				?>
				</form>
			</div>
		</div>
	</div>
	<script language="JavaScript" type="text/javascript">
	function formValidation(){

		var harga_jual =  document.getElementById('harga_jual');

		//  to check empty form fields.

		if(harga_jual.value.length == 0){
			document.getElementById('head').innerText = "Semua form harus diisi!"; //this segment displays the validation rule for all fields
			harga_jual.focus();
			return false;
		} 
		
		// Check each input in the order that it appears in the form!
		if(textNumericha(harga_jual, "* Isi form dengan angka! *")){
					
			return true;
		}
		
		
		return false;
		
	}
	
	// Harga
	// function that checks whether input text is numeric or not.
	function textNumericha(inputtext, alertMsg){
		var numericExpression = /^[0-9]+$/;
		if(inputtext.value.match(numericExpression)){
			return true;
		}else{
			document.getElementById('h1').innerText = alertMsg;  //this segment displays the validation rule for zip
			inputtext.focus();
			return false;
		}
	}
	</script>