<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Daftar Member</h1>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-6">
			<a href="dashboard.php?p=tambah_member"><button class="btn btn-success"> + Tambah Member </button></a>
		</div>
		<div class="col-lg-6">
			<?php
                //kode php ini kita gunakan untuk menampilkan pesan eror
                if (!empty($_GET['psn'])) {
                    if ($_GET['psn'] == 1) {
                        writeMsg('kprod.gagal');
                    } elseif ($_GET['psn'] == 2) {
                        writeMsg('save.sukses');
                    } elseif ($_GET['psn'] == 3) {
                        writeMsg('save.gagal');
                    } elseif ($_GET['psn'] == 4) {
                        writeMsg('update.sukses');
                    } elseif ($_GET['psn'] == 5) {
                        writeMsg('update.gagal');
                    }
                }
            ?>
		</div>
	</div>

	<br />
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-info">
				<div class="panel-heading">
					Daftar Member
				</div>

				<div class="panel-body">
					<div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-paginate">
							<thead>
								<tr>
									<th>
										<center>No.</center>
									</th>
									<th>
										<center>Nomer KTP</center>
									</th>
									<th>Nama Member</th>
									<th>
										<center>Kategori Member</center>
									</th>
									<th>
										<center>Email</center>
									</th>
									<th>
										<center>Nomer Telphone</center>
									</th>
									<th>
										<center>Alamat</center>
									</th>
									<th>
										<center>Kuota Laundry</center>
									</th>
									<th width="50px">
										<center>Aksi</center>
									</th>
								</tr>
							</thead>
							<tbody>
								<?php
                                    $jumlah_desimal = '0';
                                    $pemisah_desimal = ',';
                                    $pemisah_ribuan = '.';
                                    $sql = mysql_query('SELECT
															a.id,
															a.kode,
															a.nama,
															b.kategori AS kategori_id,
															a.alamat,
															a.no_telpon,
															a.email,
															a.stok
															FROM
															member a, kategori_member b
															WHERE
															a.kategori_id=b.id
															ORDER BY nama ASC');
                                    $no = 1;
                                    while ($row = mysql_fetch_array($sql)) {
                                        ?>
								<tr>
									<td align="center">
										<?php echo $no; ?>
									</td>
									<td align="center">
										<?php echo $row['kode']; ?>
									</td>
									<td>
										<?php echo $row['nama']; ?>
									</td>
									<td align="center">
										<?php echo $row['kategori_id']; ?>
									</td>
									<td align="center">
										<?php echo $row['email']; ?>
									</td>
									<td align="center">
										<?php echo $row['no_telpon']; ?>
									</td>
									<!-- <td align="right"><b>
											<?php //echo number_format($row['harga_jual'], $jumlah_desimal, $pemisah_desimal, $pemisah_ribuan).',-';?></b></td> -->
									<td align="center">
										<?php echo $row['alamat']; ?>
									</td>
									<td align="center">
										<?php echo $row['stok']; ?>
									</td>
									<td align="center">
										<a class="btn btn-xs btn-success"
											href="dashboard.php?p=edit_member&id=<?php echo $row['id']; ?>"><span
												class="glyphicon glyphicon-edit"></span></a>

										<a class="btn btn-xs btn-danger" href="pages/aksi/memberhapus-proses.php?id=<?php echo $row['id']; ?>"
										 onclick="if (!confirm('Apakah Anda yakin akan menghapus data ini?')) return false;"> <span class="glyphicon glyphicon-trash"></span></a>
									</td>
								</tr>
								<?php ++$no;
                                    } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>