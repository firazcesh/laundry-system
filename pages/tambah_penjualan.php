	<link href="asset/select2/select2.css" rel="stylesheet">
	<link href="asset/select2/select2-bootstrap.css" rel="stylesheet">
	<?php
	date_default_timezone_set('Asia/Jakarta');
	$tgl=date('Y-m-d');
	
	$sql=mysql_query("SELECT kode_jual from penjualan order by kode_jual DESC LIMIT 0,1");
	$data=mysql_fetch_array($sql);
	$kodeawal=substr($data['kode_jual'],3,5)+1;
		if($kodeawal<10){
			$invoice='INV0000'.$kodeawal;
			}
			elseif($kodeawal > 9 && $kodeawal <=99){
				$invoice='INV000'.$kodeawal;
			}else{
				$invoice='INV000'.$kodeawal;
			}
	?>
	<div class="container">		
		<div class="row">
			<div class="col-lg-12">
				<form class="form-horizontal" method="POST">
					<div class="form-group">
						<label for="pelanggan" class="col-lg-1 control-label"><strong>Nama : </strong></label>
						<div class="select2-wrapper col-lg-4">
							<select class="form-control select2" id="pelanggan">
								<option value="">( Pilih data Pelanggan )</option>
								<?php 
								$query = mysql_query("select * from pelanggan");
								while($row = mysql_fetch_array($query)){
									echo '<option value="'.$row['id'].'">'.$row['nama'].'</option>';
								} ?>
							</select>
						</div>
						
						<label for="tanggal" class="col-lg-2 control-label"><strong>Tanggal: </strong></label>
						<div class="col-lg-2">
							<input class="form-control" type='text' id='tanggal' value="<?php echo $tgl;?>" readonly="true">
						</div>
						
						<label for="kode_jual" class="col-lg-1 control-label"><strong>Invoice : </strong></label>
						<div class="col-lg-2">
							<input type="text" maxlength="8" class="form-control uneditable-input" id="kode_jual" value="<?php echo $invoice;?>" readonly>
						</div>
					</div>
					
					<div class="form-group">						
						<label for="catatan" class="col-lg-1 control-label"><strong>Catatan : </strong></label>
						<div class="col-lg-6">
							<textarea maxlength="300" class="form-control" rows='2' id="catatan" placeholder="Catatan ..."></textarea>
						</div>
						
						<label for="pegawai" class="col-lg-3 control-label"><strong>Petugas : </strong></label>
						<div class="col-lg-2">
							<input type="text" class="form-control" id="pegawai" value="<?php echo ($_SESSION['nama'])?>" placeholder="Total pembelian .." readonly>
						</div>
					</div>
					
					<legend>List Produk</legend>
					<div class="form-group">
						<div class="col-lg-4"><input class="form-control autocomplete_txt" data-type="productNama" type='text' id='nama' maxlength="100" placeholder="Nama Produk"></div>
						<div class="col-lg-2"><input class="form-control autocomplete_txt" data-type="productKode" type='text' id='kode' placeholder="Kode Produk" readonly><input class="hidden" type="text" id="idprod"></div>
						<div class="col-lg-2"><div class="input-group"><span class="input-group-addon">Rp</span><input class="form-control" type='text' id='harga' placeholder="Harga" readonly></div></div>
						<div class="col-lg-1"><input class="form-control" type='number' id='stok' maxlength="8" placeholder="Stok" readonly></div>
						<div class="col-lg-1"><input class="form-control" type='number' id='jumlah' maxlength="8" placeholder="Jumlah"></div>
                        <div class="col-lg-1"><button id="tambah" class="btn btn-success">Tambah</button></div>
                        <span id="status"></span>
					</div>
					
					<table id="barang" class='table table-bordered table-hover'>
						
					</table>
					
					<div class="form-actions">
						<div class="col-lg-12">
							<button class="btn btn-success" type="submit" id="proses">Proses</button>
						</div>
					</div>
				</form>
			</div>
		</div>		
	</div>
	<script type="text/javascript" src="asset/js/jquery-1.11.0.js"></script>
	<script src="asset/js/penjualan.js"></script>
	<script src="asset/select2/select2.js"></script>
	
	<script>
		$( ".select2" ).select2( { placeholder: "Select a State", maximumSelectionSize: 6 } );
		$( ":checkbox" ).on( "click", function() {
			$( this ).parent().nextAll( "select" ).select2( "enable", this.checked );
		});
		
		$( "#demonstrations" ).select2( { placeholder: "Select2 version", minimumResultsForSearch: -1 } ).on( "change", function() {
			document.location = $( this ).find( ":selected" ).val();
		} );

		$( "button[data-select2-open]" ).click( function() {
			$( "#" + $( this ).data( "select2-open" ) ).select2( "open" );
		});
	</script>