<!DOCTYPE html>
<?php 
ob_start();
?>
<page>
		<style type="text/css">
		table#barang{
			border: 2px solid darkgrey;
		}
		th{
			border-bottom: 2px solid darkgrey;
		}
		td.table-td{
			border-bottom: 2px solid darkgrey;
			border-right: 0.5px solid darkgrey;
		}
		</style>
        <h1 align="center">Detail Pembelian</h1><br><hr><br>
        <?php 
		$jumlah_desimal = "0";
		$pemisah_desimal = ",";
		$pemisah_ribuan = ".";
        $kode_beli = $_GET['kode_beli'];
		$sql = mysql_query("SELECT a.tanggal_beli, a.kode_beli, a.nota_beli, a.total_beli, a.pegawai, a.keterangan_beli, b.nama as suplier FROM pembelian a, suplier b WHERE a.suplier=b.id AND a.kode_beli='$kode_beli'");
		while ($row = mysql_fetch_array($sql)) {
		?>
		<table id="lol">
			<tr><td>Kode Beli </td><td>&nbsp; : &nbsp;</td><td><?php echo $kode_beli; ?></td></tr>
			<tr><td>Tanggal Beli </td><td>&nbsp; : &nbsp;</td><td><?php echo $row['tanggal_beli']; ?></td></tr>
			<tr><td>Nomor Nota </td><td>&nbsp; : &nbsp;</td><td><?php echo $row['nota_beli']; ?></td></tr>
			<tr><td>Supplier </td><td>&nbsp; : &nbsp;</td><td><?php echo $row['suplier']; ?></td></tr>
			<tr><td>Pegawai </td><td>&nbsp; : &nbsp;</td><td><?php echo $row['pegawai']; ?></td></tr>
			<tr><td>Total </td><td>&nbsp; : &nbsp;</td><td><b>Rp <?php echo number_format($row['total_beli'],$jumlah_desimal,$pemisah_desimal,$pemisah_ribuan).",-"; ?></b></td></tr>
			<tr><td>Keterangan </td><td>&nbsp; : &nbsp;</td><td><?php echo $row['keterangan_beli']; ?></td></tr>
		</table>
		<?php
		}
        ?>
        <br><br><br><br>
        <table id="barang" align="center" cellpadding="20px">
			<tr class="lol">
				<th align="center;" width="50px;">NO</th>
				<th align="center;" width="100px;">Kode Produk</th>
				<th align="center;" width="300px;">Nama Produk</th>
				<th align="center;" width="100px;">Harga Satuan</th>
				<th align="center;" width="70px;">Jumlah</th>
				<th align="center;" width="100px;">Total Harga</th>
			</tr>
			<?php
				$jumlah_desimal = "0";
				$pemisah_desimal = ",";
				$pemisah_ribuan = ".";
				$total=mysql_fetch_array(mysql_query("SELECT * FROM pembelian where kode_beli='$_GET[kode_beli]'"));
				$sqlDetail = mysql_query("SELECT a.produk_id, a.jumlah, a.harga, a.subtotal, b.nama, b.kode FROM detail_pembelian a, produk b WHERE a.produk_id=b.id AND kode_beli='$_GET[kode_beli]'");
				$no=1;
				while ($row = mysql_fetch_array($sqlDetail)) {
			?>
			<tr class="lol">
				<td class="table-td" align="center;"><?php echo $no; ?></td>
				<td class="table-td" align="center;"><?php echo $row['kode']; ?></td>
				<td class="table-td"><?php echo $row['nama']; ?></td>
				<td class="table-td" align="right;"><?php echo number_format($row['harga'],$jumlah_desimal,$pemisah_desimal,$pemisah_ribuan).",-"; ?></td>
				<td class="table-td" align="center;"><?php echo $row['jumlah']; ?></td>
				<td class="table-td" align="right;"><?php echo number_format($row['subtotal'],$jumlah_desimal,$pemisah_desimal,$pemisah_ribuan).",-"; ?></td>
			</tr>
			<?php $no++; } ?>
			<tr>
				<th style="background:red;" align="center;" colspan='5'>Total</th>
				<th style="background:red;" align="right;" ><?php echo number_format($total['total_beli'],$jumlah_desimal,$pemisah_desimal,$pemisah_ribuan).",-"; ?></th>
			</tr>
		</table>
</page>
<?php
    $content = ob_get_clean();

// conversion HTML => PDF
 require_once(dirname(__FILE__).'/../asset/html2pdf/html2pdf.class.php');
 try
 {
 $html2pdf = new HTML2PDF('P','A4', 'fr', false, 'ISO-8859-15');
 $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
 ob_end_clean();
 $html2pdf->Output('laporan_detail_pembelian.pdf');
 }
 catch(HTML2PDF_exception $e) { echo $e; }
?>
</html>