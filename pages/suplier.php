	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Daftar Suplier</h1>
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-6">
				<a href="dashboard.php?p=tambah_suplier"><button class="btn btn-success"> + Tambah suplier </button></a>
			</div>
			<div class="col-lg-6">
			<?php
				//kode php ini kita gunakan untuk menampilkan pesan eror
				if (!empty($_GET['psn'])) {
					if ($_GET['psn'] == 1) {
						writeMsg('kodesuplier.gagal');
						}else if ($_GET['psn'] == 2) {
							writeMsg('save.sukses');
							} else if ($_GET['psn'] == 3) {
								writeMsg('tambahsuplier.gagal');
								} else if ($_GET['psn'] == 4) {
									writeMsg('update.sukses');
									} else if ($_GET['psn'] == 5) {
										writeMsg('update.gagal');
									} 
					}
			?>
			</div>
		</div>
		
		<br />
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-info">
                    <div class="panel-heading">
                        Daftar Suplier
                    </div>

                    <div class="panel-body">
                        <div class="dataTable_wrapper">
							<table class="table table-striped table-bordered table-paginate">
								<thead>
									<tr>
										<th><center>No.</center></th>
										<th>Kode Suplier</th>
										<th>Nama Suplier</th>
										<th>Alamat</th>
										<th><center>No. Tlp</center></th>
										<th><center>Email</center></th>
										<th width="15%">Aksi</th>
									</tr>
								</thead>
								<tbody>
								<?php
									$sql = mysql_query("SELECT * FROM suplier ORDER BY kode ASC");
									$no=1;
									while ($row = mysql_fetch_array($sql)) {
								?>
									<tr>
										<td align="center"><?php echo $no; ?></td>
										<td><?php echo $row['kode']; ?></td>
										<td><?php echo $row['nama']; ?></td>
										<td><?php echo $row['alamat']; ?></td>
										<td><?php echo $row['no_tlp']; ?></td>
										<td><?php echo $row['email']; ?></td>
										<td align="center">
											<a class="btn btn-xs btn-primary" href="dashboard.php?p=edit_suplier&id=<?php echo $row['id']; ?>"><span class="glyphicon glyphicon-edit"> Edit</span></a>
										</td>
									</tr>
								<?php $no++; } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>