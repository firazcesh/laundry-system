	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Daftar Pembelian</h1>
			</div>
		</div>
	
		<div class="row">
			<div class="col-lg-6">
				<a href="dashboard.php?p=tambah_pembelian"><button class="btn btn-success"> + Tambah Pembelian </button></a>
			</div>
			<div class="col-lg-6">
			<?php
				//kode php ini kita gunakan untuk menampilkan pesan eror
				if (!empty($_GET['psn'])) {
					if ($_GET['psn'] == 1) {
						writeMsg('save.sukses');
						} else if ($_GET['psn'] == 2) {
							writeMsg('save.gagal');
							} 
					}
			?>
			</div>
		</div>
		
		<br />
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-info">
					<div class="panel-heading">Daftar pembelian</div>
					
					<div class="panel-body">
						<div class="dataTable_wrapper">
							<table class="table table-striped table-bordered table-paginate">
								<thead>
									<tr>
										<th><center>No.</center></th>
										<th><center>Tanggal</center></th>
										<th><center>Kode</center></th>
										<th><center>Nota</center></th>
										<th width="30%">Suplier</th>
										<th><center>Total</center></th>
										<th width="10%"><center>Lihat</center></th>
									</tr>
								</thead>
								<tbody>
								<?php
									$jumlah_desimal = "0";
									$pemisah_desimal = ",";
									$pemisah_ribuan = ".";
									$sql = mysql_query("SELECT a.tanggal_beli, a.kode_beli, a.nota_beli, a.total_beli, b.nama as suplier FROM pembelian a, suplier b WHERE a.suplier=b.id ORDER BY tanggal_beli ASC ");
									$no=1;
									while ($row = mysql_fetch_array($sql)) {
								?>
									<tr>
										<td align="center"><?php echo $no; ?></td>
										<td align="center"><?php echo $row['tanggal_beli']; ?></td>
										<td align="center"><a href="dashboard.php?p=lihat_pembelian&kode_beli=<?php echo $row['kode_beli']; ?>"><?php echo $row['kode_beli']; ?></a></td>
										<td align="center"><?php echo $row['nota_beli']; ?></td>
										<td><?php echo $row['suplier']; ?></td>
										<td align="right"><b><?php echo number_format($row['total_beli'],$jumlah_desimal,$pemisah_desimal,$pemisah_ribuan).",-"; ?></b></td>
										<td align="center">
												<a class="btn btn-xs btn-primary" href="dashboard.php?p=lihat_pembelian&kode_beli=<?php echo $row['kode_beli']; ?>">
												<span class="glyphicon glyphicon-eye-open"> Lihat</span></a>
										</td>
									</tr>
								<?php $no++; } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>