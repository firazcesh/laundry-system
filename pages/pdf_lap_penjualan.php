<!DOCTYPE html>
<?php
ob_start();
include 'config/serverconfig.php';
?>
<page>
	<?php 
	$tgl_a = $_GET['awal'];
	$tgl_b = $_GET['akhir'];
	?>
	<h1 align="center">Laporan Penjualan</h1>
	<h5 align="center">Periode <?php echo $tgl_a ?> s/d <?php echo $tgl_b; ?></h5>
	<hr>
	<br />
	<h2 align="center">Penjualan Periode <?php echo $tgl_a ?> s/d <?php echo $tgl_b; ?></h2>
	<table align="center" border="1">
		<tr>
			<th align="center;" width="5%;">NO</th>
			<th align="center;" width="15%;">Invoice</th>
			<th align="center;" width="15%;">Tanggal</th>
			<th align="center;" width="25%;">Pelanggan</th>
			<th align="center;" width="25%;">Pegawai</th>
			<th align="center;" width="15%;">Total Transaksi</th>
		</tr>
		<?php
			$jumlah_desimal = "0";
			$pemisah_desimal = ",";
			$pemisah_ribuan = ".";
			$nomor = 1;
			$total=mysql_fetch_array(mysql_query("select sum(total_jual) as ttl FROM penjualan WHERE tanggal_jual BETWEEN '$tgl_a' AND '$tgl_b'"));
			$sql = mysql_query("SELECT a.kode_jual, a.tanggal_jual, a.total_jual, a.catatan_jual, a.pegawai, b.nama AS pelanggan 
			FROM penjualan a, pelanggan b 
			WHERE a.pelanggan=b.id AND tanggal_jual BETWEEN '$tgl_a' AND '$tgl_b' 
			ORDER BY tanggal_jual");
			while ($row = mysql_fetch_array($sql)) {
		?>
		<tr>
			<td class="table-td" align="center;"><?php echo $nomor++; ?></td>
			<td class="table-td" align="center;"><?php echo $row['tanggal_jual']; ?></td>
			<td class="table-td" align="center;"><?php echo $row['kode_jual']; ?></td>
			<td class="table-td"><?php echo $row['pelanggan'] ?></td>
			<td class="table-td"><?php echo $row['pegawai']; ?></td>
			<td class="table-td" align="right;"><b><?php echo number_format($row['total_jual'],$jumlah_desimal,$pemisah_desimal,$pemisah_ribuan).",-"; ?></b></td>
		</tr>
		<?php
			}
		?>
		<tr>
			<td class='table-td' align="center;" colspan='5'><b>Total</b></td>
			<td class='table-td' align="center;"><b><?php echo number_format($total['ttl'],$jumlah_desimal,$pemisah_desimal,$pemisah_ribuan).",-"; ?></b></td>								
		</tr>
	</table>
	<br />
	<br />
	<hr>
	<h3 align="center">Detail Penjualan Periode <?php echo $tgl_a ?> s/d <?php echo $tgl_b; ?></h3>
	<table align="center" border="1">
		<tr>
			<th align="center;">NO</th>
			<th align="center;">Kode Invoice</th>
			<th align="center;">Kode Produk</th>
			<th align="center;">Nama Produk</th>
			<th align="center;">Jumlah</th>
			<th align="center;">Harga</th>
			<th align="center;">Subtotal</th>
		</tr>
		<?php
			$jumlah_desimal = "0";
			$pemisah_desimal = ",";
			$pemisah_ribuan = ".";
			$no = 1;
			$tot=mysql_fetch_array(mysql_query("select sum(a.subtotal) as tt, a.kode_jual, b.tanggal_jual, b.kode_jual FROM detail_penjualan a, penjualan b WHERE a.kode_jual = b.kode_jual AND b.tanggal_jual BETWEEN '$tgl_a' AND '$tgl_b'"));
			$sql = mysql_query("SELECT a.tanggal_jual, a.kode_jual, a.total_jual, a.pegawai, a.catatan_jual, a.pegawai, b.kode_jual, b.produk_id, b.harga, b.jumlah, b.subtotal, c.kode, c.nama
			FROM penjualan a, detail_penjualan b, produk c
			WHERE a.kode_jual = b.kode_jual AND b.produk_id=c.id AND a.tanggal_jual BETWEEN '$tgl_a' AND '$tgl_b' 
			ORDER BY a.kode_jual");
			while ($r = mysql_fetch_array($sql)) {
		?>
		<tr>
			<td class="table-td" align="center;"><?php echo $no++; ?></td>
			<td class="table-td" align="center;"><?php echo $r['kode_jual']; ?></td>
			<td class="table-td" align="center;"><?php echo $r['kode'] ?></td>
			<td class="table-td"><?php echo $r['nama']; ?></td>
			<td class="table-td" align="center;"><?php echo $r['jumlah']; ?></td>
			<td class="table-td" align="right;"><b><?php echo number_format($r['harga'],$jumlah_desimal,$pemisah_desimal,$pemisah_ribuan).",-"; ?></b></td>
			<td class="table-td" align="right;"><b><?php echo number_format($r['subtotal'],$jumlah_desimal,$pemisah_desimal,$pemisah_ribuan).",-"; ?></b></td>
		</tr>
		<?php
			}
		?>
		<tr>
			<td class='table-td' align="center;" colspan='6'><b>Total</b></td>
			<td class='table-td' align="center;"><b><?php echo number_format($tot['tt'],$jumlah_desimal,$pemisah_desimal,$pemisah_ribuan).",-"; ?></b></td>								
		</tr>
	</table>
</page>
<?php
    $content = ob_get_clean();

// conversion HTML => PDF
 require_once(dirname(__FILE__).'/../asset/html2pdf/html2pdf.class.php');
 try
 {
 $html2pdf = new HTML2PDF('P','A4', 'fr', false, 'ISO-8859-15');
 $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
 ob_end_clean();
 $html2pdf->Output('laporan_penjualan.pdf');
 }
 catch(HTML2PDF_exception $e) { echo $e; }
?>
</html>