<?php
  include"conn.php";
  
  $kode_barang = $_GET['kode_barang'];
  $query = mysql_query("select * from barang where kode_barang = '$kode_barang'");
  $data = mysql_fetch_array($query);
  $kode_barang = $data['kode_barang'];
  $no_reg = $data['no_reg'];
  $nama = $data['nama'];
  $bahan=$data['bahan'];
  $harga=$data['harga'];
  


 $total = mysql_num_rows( $query);    
// Panggil Library FPDF

require "fpdf.php";

// Buat class PDF
class PDF extends FPDF
{
    // Buat fungsi untuk mengatur header halaman
    function Header()
    {
        // Setting Font ('string family', 'string style', 'font size')
    	$this->SetFont('Helvetica','B',10);
        
        /* Cell : untuk menuliskan text kedalam cell
                  19  : menunjukan panjang Cell
                  0.7 : menunjukan tinggi cell
                  0   : yang pertama menunjukan Cell tanpa border, Isi 1 jika menginginkan Cell diberi border
                  0   : yang kedua menunjukan posisi text berikutnya disebelah kanan
                  C   : menunjukan text berada ditengah-tengah / Center
        */
 $this->Cell(19,0.7,'CV. BAROKAH DUA PUTRA NGANTANG',0,0,'C');
        
        // Ln : untuk membuat baris baru
        $this->Ln();
        $this->SetFont('Helvetica','B',10);         
        $this->Cell(19,0.7,'Jl. Raya Kaumrejo No.38 Ngantang - Malang',0,0,'C');
        
        $this->Ln();
        $this->SetFont('helvetica','',9);
        $this->Cell(19,0.5,'Telp. 0821 - 43531156, barokahduaputra@ymail.com',0,0,'C');
          
        // Line : untuk membuat garis              
        $this->Line(1,2.9,20,2.9);
        $this->Line(1,2.95,20,2.95);
        $this->Ln();
        $this->SetFont('Helvetica','B',9);
        $this->Cell(19,0.8,'Laporan Data Barang',0,0,'C');
    }
    
    
    // Buat fungsi untuk mengatur Footer halaman 
    function Footer()
    {
    	// Posisi 1 cm dari bawah
    	$this->SetY(-1);        
    	$this->SetFont('Arial','',8);        
        $this->Cell(12, 0.3 ,'',0,'LR','L');        
        $this->Cell(7,  0.3 ,'By: ELISTYA PURNAMASARI',0,'LR','R');
    }
}

// Fungsi Konstruktor PDF
$pdf = new PDF("P","cm", array(21, 21));

// Fungsi untuk membuat halaman
$pdf->AddPage();
 
        $pdf->Ln();
        $pdf->SetFont('Helvetica','',10);
        $pdf->Cell(3.2,0.5,'Kode Barang',0,'LR', 'L');
        $pdf->Cell(7.9,0.5,':'.$kode_barang,0,'LR', 'L');
        
        $pdf->Ln();
		
        $pdf->Cell(3.2,0.5,'No Registrasi',0,'LR', 'L');
        $pdf->Cell(7.9,0.5, ':'.$no_reg,0,'LR', 'L');
        
	
		$pdf->Ln();
		
		$pdf->Cell(3.2,0.5,'Nama',0,'LR', 'L');
        $pdf->Cell(7.9,0.5, ':'.$nama,0,'LR', 'L');
        
		$pdf->Ln();
		
        $pdf->Cell(3.2,0.5,'Bahan',0,'LR', 'L');
        $pdf->Cell(5.4,0.5, ':'.$bahan,0,'LR', 'L');
		
		$pdf->Ln();
		
		$pdf->Cell(3.2,0.5,'Harga',0,'LR', 'L');
        $pdf->Cell(5.4,0.5, ':'.$harga,0,'LR', 'L');
		

		
// Fungsi untuk setting margin halaman (kiri, atas, kanan)
$pdf->SetMargins(1,0.5,1);

$pdf->Ln();
$pdf->SetTextColor(0, 0, 0);
$pdf->Cell(19   ,0.02,'',0,'LR','C',1);

// SETTING BARIS UNTUK TANDA TANGAN
$pdf->Ln(1);
$pdf->Cell(6.333 ,0.3,'',0,'LR', 'C');
$pdf->Cell(6.333 ,0.3,'',0,'LR', 'C');

// Generate PDF
$pdf->Output();
?>