	<div class="container">
		<h3>Laporan Penjualan</h3>
		<hr>
		
		<div class="row">
			<div class="col-lg-12">
				<form class="form-horizontal" method="GET">	
					<input type="hidden" name="p" id="p"  value="laporan_penjualan"/>
					
					<div class="form-group">
						<label for="awal" class="col-lg-2 control-label">Dari Tanggal :</label>
						<div class="col-lg-2">
							<input type="date" class="form-control" id="awal" name="awal" required>
						</div>
					</div>
						
					<div class="form-group">
						<label for="akhir" class="col-lg-2 control-label">Sampai Tanggal :</label>
						<div class="col-lg-2">
							<input type="date" class="form-control" id="akhir" name="akhir" required>
						</div>
					</div>
						
					<div class="form-actions">
						<div class="col-lg-12">
							<button class="btn btn-success" type="submit" value="Search">Lihat</button>
						</div>
					</div>
				</form>
				
				<?php
					if (isset($_GET['awal'])) {
						$tgl_a = $_GET['awal'];
						$tgl_b = $_GET['akhir'];
							echo "<br /><h2 align='center'>Penjualan Periode <b>$tgl_a</b> Sampai <b>$tgl_b</b></h2><br />";
								echo "<div class='form-group form-horizontal'>
										<div class='col-lg-3 col-lg-offset-9' align='right'>
											<a href='dashboard.php?p=excel_penjualan&awal=$_GET[awal]&akhir=$_GET[akhir]'>
											<button class='btn btn-success'> Export to Excel </button></a>&nbsp;
											<a href='dashboard.php?p=pdf_lap_penjualan&awal=$_GET[awal]&akhir=$_GET[akhir]'>
											<button class='btn btn-success'> Export to PDF </button></a>
										</div>
									</div>";
								echo "<table class='table table-bordered table-hover'>";
								echo "<thead>
									<tr>
										<th width='5%'><center>No.</center></th>
										<th width='10%'><center>Tanggal</center></th>
										<th width='10%'><center>Invoice</center></th>
										<th width='30%'><center>Pelanggan</center></th>
										<th width='20%'><center>Petugas</center></th>
										<th width='10%'><center>Total Transaksi</center></th>
									</tr>
								</thead>";
								$no=1;
								$total=mysql_fetch_array(mysql_query("select sum(total_jual) as ttl FROM penjualan WHERE tanggal_jual BETWEEN '$tgl_a' AND '$tgl_b'"));
										
								$sql = mysql_query("SELECT a.kode_jual, a.tanggal_jual, a.total_jual, a.catatan_jual, a.pegawai, b.nama AS pelanggan FROM penjualan a, pelanggan b WHERE a.pelanggan=b.id AND tanggal_jual BETWEEN '$tgl_a' AND '$tgl_b' ORDER BY tanggal_jual ASC");
								$cek = mysql_num_rows($sql);
									if ($cek >= 1) {
										while ($row = mysql_fetch_array($sql)) {
								echo "<tbody>
										<tr>
											<td align='center'>$no</td>
											<td align='center'>$row[tanggal_jual]</td>
											<td align='center'><font color='blue'><a href='dashboard.php?p=lihat_penjualan&kode_jual=$row[kode_jual]'>$row[kode_jual]</a></font></td>
											<td>$row[pelanggan]</td>
											<td>$row[pegawai]</td>
											<td align='right'><b>$row[total_jual]</b></td>
										</tr>";
									$no++; }
									echo"<tr>
											<td class='success' align='center' colspan='5'><b>Total<b></td>
											<td class='danger' align='center'><b>$total[ttl]</b></td>								
										</tr>";
										}
											else{
									echo"<tr>
											<td colspan='7'><center><b>TIDAK ADA TRANSAKSI</b></center></td>
										</tr>";
										}
								echo "</tbody>";
							echo "</table>";
						}
					?>
			</div>
		</div>