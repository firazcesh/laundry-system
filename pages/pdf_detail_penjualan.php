<!DOCTYPE html>
<?php
ob_start();
?>
<page>
		<style type="text/css">
		table#barang{
			border: 2px solid darkgrey;
		}
		th{
			border-bottom: 2px solid darkgrey;
		}
		td.table-td{
			border-bottom: 2px solid darkgrey;
			border-right: 0.5px solid darkgrey;
		}
		</style>
		<table border="0" align="center" style="font-size: 16px; border-collapse: collapse; width: 100%;">
			<tr><td style="font-size: 30px; width: 90%;" align="center;"><b>Laundry System - Laundry</b></td></tr>
			<tr><td style="font-size: 18px; width: 90%;" align="center;"><b>PERDAGANGAN UMUM</b></td></tr>
			<tr><td style="font-size: 14px; width: 92%;" align="center;">Perum Pejuang Jaya Blok G No 74 RT 05 RW 015 Medan Satria, Bekasi Barat</td></tr>
			<tr><td style="font-size: 14px; width: 92%; padding:2; border-top:1;" align="center;">Tlp : 082258182621</td></tr>
		</table>
		<hr>
		<h1 align="center">KUITANSI</h1>
        <?php
        $jumlah_desimal = '0';
        $pemisah_desimal = ',';
        $pemisah_ribuan = '.';
        $kode_jual = $_GET['kode_jual'];
        $sql = mysql_query("SELECT a.kode_jual, a.tanggal_jual, a.total_jual, a.catatan_jual, a.pegawai, b.nama AS pelanggan, b.alamat, b.no_tlp FROM penjualan a, pelanggan b WHERE a.pelanggan=b.id AND kode_jual='$_GET[kode_jual]'");
        while ($row = mysql_fetch_array($sql)) {
            ?>

		<table border="0" align="right" style="font-size: 16px; border-collapse: collapse; width: 100%;">
			<tr><td><b>Invoice </b></td><td>&nbsp; : &nbsp;</td><td><?php echo $kode_jual; ?></td></tr>
			<tr><td>Tanggal </td><td>&nbsp; : &nbsp;</td><td><?php echo $row['tanggal_jual']; ?></td></tr>
		</table>

		<p style="font-size: 14px;"><b>Kuitansi Pembayaran untuk :</b></p>
		<table border="0" style="font-size: 16px; border-collapse: collapse; width: 100%;">
			<tr><td>Nama </td><td>&nbsp; : &nbsp;</td><td><?php echo $row['pelanggan']; ?></td></tr>
			<tr><td>Alamat </td><td>&nbsp; : &nbsp;</td><td><?php echo $row['alamat']; ?></td></tr>
			<tr><td>Telepon </td><td>&nbsp; : &nbsp;</td><td><?php echo $row['no_tlp']; ?></td></tr>
			<tr><td>Total Bayar</td><td>&nbsp; : &nbsp;</td><td><b>Rp <?php echo number_format($row['total_jual'], $jumlah_desimal, $pemisah_desimal, $pemisah_ribuan).',-'; ?></b></td></tr>
			<tr><td>Keterangan </td><td>&nbsp; : &nbsp;</td><td><?php echo $row['catatan_jual']; ?></td></tr>
		</table>
		<?php
        }
        ?>
        <br><br><br><br>
        <table id="barang" align="center" cellpadding="20px">
			<tr class="lol">
				<th align="center;" width="50px;">NO</th>
				<th align="center;" width="100px;">Kode Produk</th>
				<th align="center;" width="300px;">Nama Produk</th>
				<th align="center;" width="100px;">Harga Satuan</th>
				<th align="center;" width="70px;">Jumlah</th>
				<th align="center;" width="100px;">Total</th>
			</tr>
			<?php
                $jumlah_desimal = '0';
                $pemisah_desimal = ',';
                $pemisah_ribuan = '.';
                $total = mysql_fetch_array(mysql_query("select * from penjualan where kode_jual='$_GET[kode_jual]'"));
                $sqlDetail = mysql_query("SELECT a.produk_id, a.harga, a.jumlah, a.subtotal, b.nama, b.kode FROM detail_penjualan a, produk b WHERE a.produk_id=b.id AND kode_jual='$_GET[kode_jual]'");
                $no = 1;
                while ($row = mysql_fetch_array($sqlDetail)) {
                    ?>
			<tr class="lol">
				<td class="table-td" align="center;"><?php echo $no; ?></td>
				<td class="table-td" align="center;"><?php echo $row['kode']; ?></td>
				<td class="table-td"><?php echo $row['nama']; ?></td>
				<td class="table-td" align="right;"><?php echo number_format($row['harga'], $jumlah_desimal, $pemisah_desimal, $pemisah_ribuan).',-'; ?></td>
				<td class="table-td" align="center;"><?php echo $row['jumlah']; ?></td>
				<td class="table-td" align="right;"><?php echo number_format($row['subtotal'], $jumlah_desimal, $pemisah_desimal, $pemisah_ribuan).',-'; ?></td>
			</tr>
			<?php ++$no;
                } ?>
			<tr>
				<th style="background:red;" align="center;" colspan='5'>Total</th>
				<th style="background:red;" align="right;" ><?php echo number_format($total['total_jual'], $jumlah_desimal, $pemisah_desimal, $pemisah_ribuan).',-'; ?></th>
			</tr>
		</table>
		<br /><br /><br /><br />
		<?php
        $kode_jual = $_GET['kode_jual'];
        $sql = mysql_query("SELECT * FROM penjualan WHERE kode_jual='$kode_jual'");
        while ($row = mysql_fetch_array($sql)) {
            ?>
		<table border="0" align="right" style="font-size: 16px; border-collapse: collapse; width: 100%;">
			<tr><td style="width: 100%; padding: 2;" align="right;">Jakarta, <?php echo $row['tanggal_jual']; ?></td></tr>
			<tr><td style="width: 100%; padding: 2;" align="right;">Petugas,</td></tr>
			<tr><td style="width: 100%; padding: 2;" align="right;">&nbsp;<br/>&nbsp;<br/>&nbsp;</td></tr>
			<tr><td style="width: 100%; padding: 2;" align="right;">(<?php echo $row['pegawai']; ?>)</td></tr>
		</table>

		<?php
        }
        ?>
</page>
<?php
    $content = ob_get_clean();

// conversion HTML => PDF
 require_once dirname(__FILE__).'/../asset/html2pdf/html2pdf.class.php';
 try {
     $html2pdf = new HTML2PDF('P', 'A4', 'fr', false, 'ISO-8859-15');
     $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
     ob_end_clean();
     $html2pdf->Output('laporan_detail_penjualan.pdf');
 } catch (HTML2PDF_exception $e) {
     echo $e;
 }
?>
</html>