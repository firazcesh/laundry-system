<?php
$sql = mysql_query("SELECT * from detail_penjualan where kode_jual='$_GET[kode_jual]'");
$row = mysql_fetch_array($sql);
?>	
<div class="container">
<h3>Detail Penjualan</h3>
<i class="icon glyphicon glyphicon-home"></i><a href="dashboard.php"> Beranda</a> > <a href="dashboard.php?p=penjualan"> Histori Penjualan </a> > <b>Detail Penjualan</b>
<hr/>
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-info">
            <div class="panel-heading"><b>Invoice <?php echo $_GET['kode_jual']; ?></b></div>
            <div class="panel-body">
			<?php
				$jumlah_desimal = "0";
				$pemisah_desimal = ",";
				$pemisah_ribuan = ".";
				$sqlHeader = mysql_query("SELECT a.kode_jual, a.tanggal_jual, a.total_jual, a.catatan_jual, a.pegawai, b.nama AS pelanggan FROM penjualan a, pelanggan b WHERE a.pelanggan=b.id AND kode_jual='$_GET[kode_jual]'");
				$no=1;
				while ($row = mysql_fetch_array($sqlHeader)) {
			?>
				<table border="0px">
					<tr><td width="10%">Kode Invoice </td><td width="3%">&nbsp; : &nbsp;</td><td width="90%"><?php echo $row['kode_jual']; ?></td>
						<td align="right" valign="top" width="10%" rowspan="6">
							&nbsp;<a href="dashboard.php?p=pdf_detail_penjualan&kode_jual=<?php echo $_GET['kode_jual'] ?>"><button class="btn btn-success btn-xs"> Cetak Kuitansi </button></a>
						</td></tr>
					<tr><td>Tanggal Jual </td><td>&nbsp; : &nbsp;</td><td><?php echo $row['tanggal_jual']; ?></td></tr>
					<tr><td>Pelanggan </td><td>&nbsp; : &nbsp;</td><td><?php echo $row['pelanggan']; ?></td></tr>
					<tr><td>Pegawai </td><td>&nbsp; : &nbsp;</td><td><?php echo $row['pegawai']; ?></td></tr>
					<tr><td>Total Jual </td><td>&nbsp; : &nbsp;</td><td><b>Rp <?php echo number_format($row['total_jual'],$jumlah_desimal,$pemisah_desimal,$pemisah_ribuan).",-"; ?></b></td></tr>
					<tr><td>Keterangan </td><td>&nbsp; : &nbsp;</td><td><?php echo $row['catatan_jual']; ?></td></tr>
				</table>
				<?php } ?>
				
            <br/>
			
			<div class="dataTable_wrapper">
				<table class="table table-striped table-bordered ">
					<thead>
						<tr>
							<th><center>No.</center></th>
							<th><center>Kode Produk</center></th>
							<th>Nama Produk</th>
							<th><center>Harga </center></th>
							<th><center>Jumlah Jual</center></th>
							<th><center>Subtotal</center></th>
						</tr>
					</thead>
					<tbody>
					<?php
					$jumlah_desimal = "0";
					$pemisah_desimal = ",";
					$pemisah_ribuan = ".";
					$total=mysql_fetch_array(mysql_query("select * from penjualan where kode_jual='$_GET[kode_jual]'"));
					$sqlDetail = mysql_query("SELECT a.produk_id, a.harga, a.jumlah, a.subtotal, b.nama, b.kode FROM detail_penjualan a, produk b WHERE a.produk_id=b.id AND kode_jual='$_GET[kode_jual]'");
					$no=1;
					while ($detail = mysql_fetch_array($sqlDetail)) {
					?>
						<tr>
							<td align="center"><?php echo $no; ?></td>
							<td align="center"><?php echo $detail['kode']; ?></a></font></td>
							<td><?php echo $detail['nama']; ?></td>
							<td align="right"><b><?php echo number_format($detail['harga'],$jumlah_desimal,$pemisah_desimal,$pemisah_ribuan).",-"; ?></b></td>
							<td align="center"><?php echo $detail['jumlah']; ?></td>
							<td align="right"><b><?php echo number_format($detail['subtotal'],$jumlah_desimal,$pemisah_desimal,$pemisah_ribuan).",-"; ?></b></td>
						</tr>
						<?php $no++; } ?>
						<tr>
							<th class="success" colspan='5'><center>Total</center></th>
							<th class="danger" colspan='2' ><center><b><?php echo number_format($total['total_jual'],$jumlah_desimal,$pemisah_desimal,$pemisah_ribuan).",-"; ?></b><center></th>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
</div>