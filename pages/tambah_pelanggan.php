	<?php
	//Kode ini untuk mengurutkan otomatis kode pelanggan
		$sql=mysql_query("SELECT kode from pelanggan order by kode DESC LIMIT 0,1");
		$data=mysql_fetch_array($sql);
		$kodeawal=substr($data['kode'],2,4)+1;
			if($kodeawal<10){
				$kode='PL000'.$kodeawal;
				}
				elseif($kodeawal > 9 && $kodeawal <=99){
					$kode='PL00'.$kodeawal;
					}else{
						$kode='PL00'.$kodeawal;
					}
	?>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Tambah Pelanggan</h1>
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-3">
				<a href="dashboard.php?p=pelanggan"><button class="btn btn-danger"><span class="glyphicon glyphicon-arrow-left"> Kembali</span></button></a>
			</div>
			<div class="col-lg-4">
				<span id="head" class="label label-success"></span>
			</div>
		</div>
		
		<br />

		<div class="row">
			<div class="col-lg-12">
				<form class="form-horizontal" method="POST" onsubmit='return formValidation()'>
					<div class="form-group">
						<label for="kode" class="col-lg-2 control-label">Kode Pelanggan </label>
						<div class="col-lg-2">
							<input type="text" maxlength="100" class="form-control" id="kode" name="kode" value="<?php echo $kode; ?>" readonly>
						</div>
					</div>
					
					<div class="form-group">
						<label for="nama" class="col-lg-2 control-label">Nama </label>
						<div class="col-lg-6">
							<input type="text" maxlength="100" class="form-control" id="nama" name="nama" placeholder="nama pelanggan ..." >
						</div>
						<span id="nm1" class="label label-success"></span><span id="nm2" class="label label-warning"></span>
					</div>
					
					<div class="form-group">
						<label for="alamat" class="col-lg-2 control-label">Alamat </label>
						<div class="col-lg-6">
							<textarea maxlength="300" class="form-control" id="alamat" name="alamat" required></textarea>
						</div>
					</div>
					
					<div class="form-group">
						<label for="no_tlp" class="col-lg-2 control-label">No. Telepon </label>
						<div class="col-lg-6">
							<input type="text" maxlength="12" class="form-control" id="no_tlp" name="no_tlp" placeholder="Nomor telepon ..." > <br />
						</div>
						<span id="nt1" class="label label-success"></span><span id="nt2" class="label label-warning"></span>
					</div>
					
					<div class="form-group">
						<div class="col-lg-10 col-lg-offset-2">
							<button class="btn btn-danger" type="reset" value="Reset" >Reset</button>
							<button class="btn btn-success" type="submit" value="Simpan" name="simpan">Simpan</button>
						</div>
					</div>
					<?php
						if (isset($_POST["simpan"])) {

								$query="INSERT INTO pelanggan (kode, nama, alamat, no_tlp) VALUES ('$_POST[kode]', '$_POST[nama]', '$_POST[alamat]', '$_POST[no_tlp]')";
								$sql = mysql_query($query); 
							
								if ($sql){
									header('Location: dashboard.php?p=pelanggan&psn=1');
								}else{
									header('Location: dashboard.php?p=pelanggan&psn=2');
								}					
						}
					?>
				</form>
			</div>
		</div>
	</div>
	<script language="JavaScript" type="text/javascript">
	function formValidation(){

		// Make quick references to our fields	
		
		var nama =  document.getElementById('nama');
		var alamat =  document.getElementById('alamat');
		var no_tlp =  document.getElementById('no_tlp');

		//  to check empty form fields.

		if(nama.value.length == 0){
			document.getElementById('head').innerText = "Semua form harus diisi!"; //this segment displays the validation rule for all fields
			nama.focus();
			return false;
		} 
		
		if(textAlphanumericspace(nama, "Isi form tanpa karakter spesial")){
			
			if(lengthDefinenm(nama, 5, 100)){
				
				if(textNumericst(no_tlp, "Isi form dengan angka!")){
							
					if(lengthDefinent(no_tlp, 7, 12)){
					
					return true;
					}
				}
			}
		}
		
		return false;
		
	}
	
	//Nama
	//function that checks whether input text includes alphabetic and numeric characters.
	function textAlphanumericspace(inputtext, alertMsg){
		var alphaExp = /^[0-9a-zA-Z .()]+$/;
		if(inputtext.value.match(alphaExp)){
			return true;
		}else{
			document.getElementById('nm1').innerText = alertMsg; //this segment displays the validation rule for address
			inputtext.focus();
			return false;
		}
	}
	function lengthDefinenm(inputtext, min, max){
		var uInput = inputtext.value;
		if(uInput.length >= min && uInput.length <= max){
			return true;
		}else{
			
			document.getElementById('nm2').innerText = "* Masukkan " +min+ " sampai " +max+ " karakter *"; 
			inputtext.focus();
			return false;
		}
	}

	
	//No. Tlp
	function textNumericst(inputtext, alertMsg){
		var numericExpression = /^[0-9]+$/;
		if(inputtext.value.match(numericExpression)){
			return true;
		}else{
			document.getElementById('nt1').innerText = alertMsg;  //this segment displays the validation rule for zip
			inputtext.focus();
			return false;
		}
	}
	// Function that checks whether the input characters are restricted according to defined by user.
	function lengthDefinent(inputtext, min, max){
		var uInput = inputtext.value;
		if(uInput.length >= min && uInput.length <= max){
			return true;
		}else{
			
			document.getElementById('nt2').innerText = "* Masukkan " +min+ " sampai " +max+ " karakter *"; 
			inputtext.focus();
			return false;
		}
	}
	</script>