<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Daftar Penjualan</h1>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-6">
				<a href="dashboard.php?p=tambah_penjualan"><button class="btn btn-success"> + Tambah Penjualan </button></a>
			</div>
			<div class="col-lg-6">
			<?php
                //kode php ini kita gunakan untuk menampilkan pesan eror
                if (!empty($_GET['psn'])) {
                    if ($_GET['psn'] == 1) {
                        writeMsg('save.sukses');
                    } elseif ($_GET['psn'] == 2) {
                        writeMsg('save.gagal');
                    }
                }
            ?>
			</div>
		</div>

		<br />
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-info">
					<div class="panel-heading">Daftar penjualan</div>

					<div class="panel-body">
						<div class="dataTable_wrapper">
							<table class="table table-striped table-bordered table-paginate">
								<thead>
									<tr>
										<th><center>No.</center></th>
										<th><center>Tanggal</center></th>
										<th><center>Kode Invoice</center></th>
										<th width="30%">Pelanggan</center></th>
										<th><center>Petugas</center></th>
										<th><center>Total</center></th>
										<th width="10%"><center>Lihat</center></th>
									</tr>
								</thead>
								<tbody>
								<?php
                                    $jumlah_desimal = '0';
                                    $pemisah_desimal = ',';
                                    $pemisah_ribuan = '.';
                                    $sql = mysql_query('SELECT a.kode_jual, a.tanggal_jual, a.total_jual, a.catatan_jual, a.pegawai, b.nama AS pelanggan FROM penjualan a, pelanggan b WHERE a.pelanggan=b.id ');
                                    $no = 1;
                                    while ($row = mysql_fetch_array($sql)) {
                                        ?>
									<tr>
										<td align="center"><?php echo $no; ?></td>
										<td align="center"><?php echo $row['tanggal_jual']; ?></td>
										<td align="center"><a href="dashboard.php?p=lihat_penjualan&kode_jual=<?php echo $row['kode_jual']; ?>"><?php echo $row['kode_jual']; ?></a></td>
										<td><?php echo $row['pelanggan']; ?></td>
										<td><?php echo $row['pegawai']; ?></td>
										<td align="right"><b><?php echo number_format($row['total_jual'], $jumlah_desimal, $pemisah_desimal, $pemisah_ribuan).',-'; ?></b></td>
										<td align="center">
											<a class="btn btn-xs btn-primary" href="dashboard.php?p=lihat_penjualan&kode_jual=<?php echo $row['kode_jual']; ?>">
											<span class="glyphicon glyphicon-eye-open"> Lihat
											</span></a>
										</td>
									</tr>
								<?php ++$no;
                                    } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>