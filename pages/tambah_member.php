<script src="asset/js/jquery.min.js"></script>
	<script>
	// Script chek kode member real time
	$(document).ready(function() {
		$('#kode').change(function() {  // Jika terjadi perubahan pada id kode
			var kode = $(this).val(); // Ciptakan variabel kode untuk menampung alamat kode yang diinputkan
			$.ajax({ // Lakukan pengiriman data dengan Ajax
				type: 'POST', // dengan tipe pengiriman POST
				url: 'pages/aksi/cek_kode.php', // dan kirimkan prosesnya ke file cek-kode.php
				data: 'kode=' + kode,  // datanya ialah data kode

				success: function(response) { // Jika berhasil
					$('.hasil-kode').html(response); // Tampilkan pesan ke class hasil-kode
				}
			});
		});
	});
	</script>

	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Tambah Member</h1>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-2">
				<a href="dashboard.php?p=member"><button class="btn btn-danger"><span class="glyphicon glyphicon-arrow-left"> Kembali</span></button></a>
			</div>
			<div class="col-lg-4">
				<span id="head" class="label label-success"></span>
			</div>
		</div>

		<br />

		<div class="row">
			<div class="col-lg-12">
				<form class="form-horizontal" method="POST" onsubmit='return formValidation()'>
					<div class="form-group">
						<label for="kode" class="col-lg-2 control-label">Nomer KTP : </label>
						<div class="col-lg-2">
							<input type="text" maxlength="17" class="form-control" id="kode" name="kode" placeholder="Nomer KTP ..."> <span class="hasil-kode"></span>
						</div>
						<span id="p1" class="label label-success"></span><span id="p2" class="label label-primary"></span>
					</div>

					<div class="form-group">
						<label for="nama" class="col-lg-2 control-label">Nama Member : </label>
						<div class="col-lg-6">
							<input type="text" maxlength="100" class="form-control" id="nama" name="nama" placeholder="Nama Member ...">
						</div>
						<span id="n1" class="label label-success"></span><span id="n2" class="label label-primary"></span>
					</div>

					<div class="form-group">
						<label for="alamat" class="col-lg-2 control-label">Alamat : </label>
						<div class="col-lg-6">
							<textarea  maxlength="300" class="form-control" id="alamat" name="alamat"></textarea>
						</div>
					</div>

					<div class="form-group">
						<label for="kategori_id" class="col-lg-2 control-label">Kategori : </label>
						<div class="col-lg-6">
							<select class="form-control" id="kategori_id" name="kategori_id">
								<option value="">( Pilih Kategori )</option>
								<?php
                                $query = mysql_query('SELECT * FROM kategori_member ORDER by kategori ASC');
                                while ($row = mysql_fetch_array($query)) {
                                    echo '<option value="'.$row['id'].'">'.$row['kategori'].'</option>';
                                } ?>
							</select>
						</div>
						<span id="sl1" class="label label-success"></span>
					</div>

					<div class="form-group">
					<label for="email" class="col-lg-2 control-label">Email : </label>
						<div class="col-lg-6">
							<input type="text" maxlength="100" class="form-control" id="email" name="email" placeholder="Email ...">
						</div>
						<span id="n1" class="label label-success"></span><span id="n2" class="label label-primary"></span>
					</div>
					<div class="form-group">
						<label for="no_telpon" class="col-lg-2 control-label">Nomer Telphone : </label>
						<div class="col-lg-6">
							<input type="text" maxlength="15" class="form-control" id="no_telpon" name="no_telpon" placeholder="Nomer Telphone ..."> <br />
						</div>
						<span id="c1" class="label label-success"></span>
					</div>

					<div class="form-group">
						<label for="stok" class="col-lg-2 control-label">Kuota : </label>
						<div class="col-lg-6">
							<input type="text" maxlength="8" class="form-control" id="stok" name="stok" placeholder="Kuota ..."> <br />
						</div>
						<span id="c2" class="label label-success"></span>
					</div>

					<div class="form-group">
						<label for="satuan" class="col-lg-2 control-label">Satuan : </label>
						<div class="col-lg-6">
							<select class="form-control" id="satuan" name="satuan">
								<option value="">( Pilih Satuan )</option>
								<option value="KG"> KG </option>
								<option value="PCS"> PCS </option>
								<option value="Unit"> Unit </option>
							</select>
						</div>
						<span id="stu" class="label label-success"></span>
					</div>

					<div class="form-group">
						<div class="col-lg-10 col-lg-offset-2">
							<button class="btn btn-danger" type="reset" value="Reset" >Reset</button>
							<button class="btn btn-success" type="submit" value="Simpan" name="simpan">Simpan</button>
						</div>
					</div>
					<?php
                    if (isset($_POST['simpan'])) {
                        $cekdata = "SELECT kode from member WHERE kode='$_POST[kode]'";
                        $ada = mysql_query($cekdata) or die(mysql_error());

                        if (mysql_num_rows($ada) > 0) {
                            header('Location: dashboard.php?p=member&psn=1');
                        } else {
                            $query = "INSERT INTO member (kode, nama, alamat, kategori_id, email, no_telpon, stok, satuan) VALUES ('$_POST[kode]', '$_POST[nama]', '$_POST[alamat]', '$_POST[kategori_id]', '$_POST[email]', '$_POST[no_telpon]', '$_POST[stok]', '$_POST[satuan]')";
                            $sql = mysql_query($query);

                            if ($sql) {
                                header('Location: dashboard.php?p=member&psn=2');
                            } else {
                                header('Location: dashboard.php?p=member&psn=3');
                            }
                        }
                    }
                ?>
				</form>
			</div>
		</div>
	</div>
	<script language="JavaScript" type="text/javascript">
	function formValidation(){

		// Make quick references to our fields
		var kode =  document.getElementById('kode');
		var nama =  document.getElementById('nama');
		var no_telpon =  document.getElementById('no_telpon');
		var stok =  document.getElementById('stok');
		var merek_id =  document.getElementById('merek_id');
		var satuan =  document.getElementById('satuan');
		var kategori_id =  document.getElementById('kategori_id');

		//  to check empty form fields.

		if(kode.value.length == 0){
			document.getElementById('head').innerText = "Semua form harus diisi!"; //this segment displays the validation rule for all fields
			kode.focus();
			return false;
		}

		// Check each input in the order that it appears in the form!
		if(textAlphanumeri(kode, "* Kode tanpa spesial karakter dan spasi *")){

			if(lengthDefine(kode, 3, 17)){

				if(textAlphanumericspace(nama, "Karakter tidak diijinkan!")){

					if(lengthDefinenm(nama, 5, 50)){

						if(trueSelectionka(kategori_id, "* Pilih salah satu! ")){

							if(trueSelectionme(merek_id, "* Pilih salah satu! ")){

								if(textNumericha(no_telpon, "* Isi form dengan angka! *")){

									if(textNumericst(stok, "* Isi form dengan angka! ")){

										if(trueSelectionstu(satuan, "* Pilih salah satu! ")){

										return true;
										}
									}
								}
							}
						}
					}
				}
			}
		}


		return false;

	}

	//function that checks whether input text includes alphabetic and numeric characters.
	function textAlphanumericspace(inputtext, alertMsg){
		var alphaExp = /^[0-9a-zA-Z ]+$/;
		if(inputtext.value.match(alphaExp)){
			return true;
		}else{
			document.getElementById('n1').innerText = alertMsg; //this segment displays the validation rule for address
			inputtext.focus();
			return false;
		}
	}
	// Function that checks whether the input characters are restricted according to defined by user.
	function lengthDefinenm(inputtext, min, max){
		var uInput = inputtext.value;
		if(uInput.length >= min && uInput.length <= max){
			return true;
		}else{

			document.getElementById('n2').innerText = "* Masukkan " +min+ " sampai " +max+ " karakter *"; //this segment displays the validation rule for username
			inputtext.focus();
			return false;
		}
	}

	//function that checks whether input text includes alphabetic and numeric characters.
	function textAlphanumeri(inputtext, alertMsg){
		var alphaExp = /^[0-9a-zA-Z-]+$/;
		if(inputtext.value.match(alphaExp)){
			return true;
		}else{
			document.getElementById('p1').innerText = alertMsg; //this segment displays the validation rule for address
			inputtext.focus();
			return false;
		}
	}

	// Function that checks whether the input characters are restricted according to defined by user.
	function lengthDefine(inputtext, min, max){
		var uInput = inputtext.value;
		if(uInput.length >= min && uInput.length <= max){
			return true;
		}else{

			document.getElementById('p2').innerText = "* Masukkan " +min+ " sampai " +max+ " karakter *"; //this segment displays the validation rule for username
			inputtext.focus();
			return false;
		}
	}


	// Harga
	// function that checks whether input text is numeric or not.
	function textNumericha(inputtext, alertMsg){
		var numericExpression = /^[0-9]+$/;
		if(inputtext.value.match(numericExpression)){
			return true;
		}else{
			document.getElementById('c1').innerText = alertMsg;  //this segment displays the validation rule for zip
			inputtext.focus();
			return false;
		}
	}
	// Stok
	// function that checks whether input text is numeric or not.
	function textNumericst(inputtext, alertMsg){
		var numericExpression = /^[0-9]+$/;
		if(inputtext.value.match(numericExpression)){
			return true;
		}else{
			document.getElementById('c2').innerText = alertMsg;  //this segment displays the validation rule for zip
			inputtext.focus();
			return false;
		}
	}



	// Function that checks whether a option is selected from the selector and if it's not it displays an alert message.
	// Kategori
	function trueSelectionka(inputtext, alertMsg){
		if(inputtext.value == ""){
			document.getElementById('sl1').innerText = alertMsg; //this segment displays the validation rule for selection
			inputtext.focus();
			return false;
		}else{
			return true;
		}
	}
	// Function that checks whether a option is selected from the selector and if it's not it displays an alert message.
	// Merek
	function trueSelectionme(inputtext, alertMsg){
		if(inputtext.value == ""){
			document.getElementById('sl2').innerText = alertMsg; //this segment displays the validation rule for selection
			inputtext.focus();
			return false;
		}else{
			return true;
		}
	}
	// Function that checks whether a option is selected from the selector and if it's not it displays an alert message.
	// Suplier
	function trueSelectionsu(inputtext, alertMsg){
		if(inputtext.value == ""){
			document.getElementById('sl3').innerText = alertMsg; //this segment displays the validation rule for selection
			inputtext.focus();
			return false;
		}else{
			return true;
		}
	}

	//function that checks whether input text is an alphabetic character or not.
	function trueSelectionstu(inputtext, alertMsg){
		if(inputtext.value == ""){
			document.getElementById('stu').innerText = alertMsg; //this segment displays the validation rule for selection
			inputtext.focus();
			return false;
		}else{
			return true;
		}
	}
	</script>