	<link href="asset/datetimepicker/bootstrap-datetimepicker.min.css" rel="stylesheet">
	<link href="asset/css/bootstrap.min.css" rel="stylesheet">
	<link href="asset/js/jqueryui/jquery-ui.min.css" rel="stylesheet">
	<link href="asset/js/autocomplete/autocomplete-0.3.0.css" rel="stylesheet">
	
	<script type="text/javascript" src="asset/js/jquery-1.11.0.js"></script>
	<script type="text/javascript" src="asset/js/jquery.min.js"></script>
	<script type="text/javascript" src="asset/js/jqueryui/jquery-ui.min.js"></script>
	<script type="text/javascript" src="asset/js/autocomplete/autocomplete-0.3.0.js"></script>
	<script type="text/javascript" src="asset/datetimepicker/bootstrap-datetimepicker.js" charset="UTF-8"></script>
	<script type="text/javascript" src="asset/datetimepicker/bootstrap-datetimepicker.id.js" charset="UTF-8"></script>
	<?php
	$sql=mysql_query("SELECT kode from pembelian order by kode DESC LIMIT 0,1");
	$data=mysql_fetch_array($sql);
	$kodeawal=substr($data['kode'],3,5)+1;
		if($kodeawal<10){
			$kode='BLJ0000'.$kodeawal;
			}
			elseif($kodeawal > 9 && $kodeawal <=99){
				$kode='BLJ000'.$kodeawal;
			}else{
				$kode='BLJ000'.$kodeawal;
			}
	?>				
	<div class="container">		
		<div class="row">
			<div class="col-lg-12">
				<form class="form-horizontal" method="POST">				
					<div class="form-group">
						<label for="tanggalbeli" class="col-lg-2 control-label">Tanggal Beli : </label>
						<div class="col-lg-2">
							<input type="date" class="form-control" id="tanggalbeli" placeholder="Tanggal ..">
						</div>
						
						<label for="totalpembelian" class="col-lg-1 col-lg-offset-4 control-label">Total Beli : </label>
						<div class="col-lg-3">
							<div class="input-group"><span class="input-group-addon">Rp</span><input type="text" class="form-control" id="totalpembelian" name='totalpembelian' placeholder="Total pembelian .."></div>
						</div>
					</div>
								
					<div class="form-group">
						<label for="kode" class="col-lg-2 control-label">Kode : </label>
						<div class="col-lg-2">
							<input type="text" maxlength="8" class="form-control uneditable-input" id="kode" name="kode" value="<?php echo $kode;?>"readonly="true" >
						</div>
								
						<label for="nota" class="col-lg-1 control-label">Nota Beli : </label>
						<div class="col-lg-2">
							<input type="text" maxlength="50" class="form-control" id="nota" name="nota" placeholder="Nota beli ..." required>
						</div>
									
						<label for="suplier" class="col-lg-1 control-label">Suplier : </label>
						<div class="col-lg-4">
							<select class="form-control" id="suplier_id" name="suplier_id">
								<option value="">Pilih Suplier</option>
								<?php 
								$query = mysql_query("SELECT * FROM suplier");
								while($row = mysql_fetch_array($query)){
									echo '<option value="'.$row['id'].'">'.$row['nama'].'</option>';
								} ?>
							</select>
						</div>
					</div>
					
					<div class="form-group">
						<label for="keterangan" class="col-lg-2 col-lg-offset-4 control-label">Keterangan : </label>
						<div class="col-lg-6">
							<textarea  maxlength="255" rows="3" class="form-control" id="keterangan" name="keterangan" required><?php echo $row['alamat']  ;?></textarea>
						</div>
					</div>
				</form>
			</div>
		</div>	
	</div>
					
	<div class="container">		
		<div class="row">
			<div class="col-lg-12">
				<form id='students' method='post' name='students' action='index.php'>
					<div class="table-responsive">
					  	<table class="table table-bordered">
							<tr>
							    <th width="2%"><input id="check_all" class="formcontrol" type="checkbox"/></th>
							    <th width="50px">No</th>
								<th width="300px">Nama Produk</th>
							    <th width="150px">Kode Produk</th>
							    <th width="100px">Jumlah Beli</th>
							    <th width="100px">Harga Beli</th>
							    <th width="100px">Total Beli</th>
							</tr>
							<tr>
						    	<td><input type='checkbox' class='case'/></td>
								<td><span id='snum'>1.</span></td>
						   	 	<td><input class="form-control autocomplete_txt" data-type="productNama" type='text' id='namaproduk_1' name='namaproduk[]' autocomplete="off"/></td>
						    	<td><input class="form-control autocomplete_txt" data-type="productKode" type='text' id='kodeproduk_1' name='kodeproduk[]' autocomplete="off"/></td>
						    	<td><input class="form-control changesNo" type='text' id='jumlahbeli_1' name='jumlahbeli[]' autocomplete="off" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;"/></td>
						    	<td><div class="input-group"><span class="input-group-addon">Rp</span><input class="form-control changesNo" type='text' id='hargabeli_1' name='hargabeli[]' autocomplete="off" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;"/> </div></td>
								<td><div class="input-group"><span class="input-group-addon">Rp</span><input class="form-control totalLinePrice" type='text' id='totalbeli_1' name='totalbeli[]' autocomplete="off" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;"/> </div></td>
						  	</tr>
					  	</table>
					</div>
				</form>
			</div>
		</div>	
	</div>
	
	<div class="container">		
		<div class="row">
			<div class="col-lg-12">
					<button type="button" class='btn btn-danger delete'><span class="glyphicon glyphicon-trash"> Delete</span></button>
					<button type="button" class='btn btn-success addmore'><span class="glyphicon glyphicon-plus">  Tambah</span></button>
					<button type="button" class='btn btn-info save'>Simpan</button>
					
				
			</div>
		</div>	
	</div>
	<script src="asset/js/atodinamis.js"></script>