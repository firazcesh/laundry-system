<?php
$sql = mysql_query("SELECT * from detail_pembelian where kode_beli='$_GET[kode_beli]'");
$row = mysql_fetch_array($sql);
?>	
<div class="container">
<h3>Detail Pembelian</h3>
<i class="icon glyphicon glyphicon-home"></i><a href="dashboard.php"> Beranda</a> > <a href="dashboard.php?p=pembelian"> Histori Pembelian </a> > <b>Detail Pembelian</b>
<hr/>
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-info">
            <div class="panel-heading"><b>Kode beli <?php echo $_GET['kode_beli']; ?></b></div>
            <div class="panel-body">
			<?php
				$jumlah_desimal = "0";
				$pemisah_desimal = ",";
				$pemisah_ribuan = ".";
				$sqlHeader = mysql_query("SELECT a.tanggal_beli, a.kode_beli, a.nota_beli, a.total_beli, a.pegawai, a.keterangan_beli, b.nama as suplier FROM pembelian a, suplier b WHERE a.suplier=b.id AND a.kode_beli='$_GET[kode_beli]'");
				$no=1;
				while ($row = mysql_fetch_array($sqlHeader)) {
			?>
				<table border="0px">
					<tr><td width="15%">Kode Beli </td><td width="3%">&nbsp; : &nbsp;</td><td width="90%"><?php echo $row['kode_beli']; ?></td>
						<td align="right" valign="top" width="10%" rowspan="7">
							<a href="dashboard.php?p=pdf_detail_pembelian&kode_beli=<?php echo $_GET['kode_beli'] ?>">
								<button class="btn btn-success btn-xs"> Cetak </button></a>
						</td></tr>
					<tr><td>Tanggal Beli </td><td>&nbsp; : &nbsp;</td><td><?php echo $row['tanggal_beli']; ?></td></tr>
					<tr><td>Nomor Nota </td><td>&nbsp; : &nbsp;</td><td><?php echo $row['nota_beli']; ?></td></tr>
					<tr><td>Supplier </td><td>&nbsp; : &nbsp;</td><td><?php echo $row['suplier']; ?></td></tr>
					<tr><td>Pegawai </td><td>&nbsp; : &nbsp;</td><td><?php echo $row['pegawai']; ?></td></tr>
					<tr><td>Total yang dibayar </td><td>&nbsp; : &nbsp;</td><td><b>Rp <?php echo number_format($row['total_beli'],$jumlah_desimal,$pemisah_desimal,$pemisah_ribuan).",-"; ?></b></td></tr>
					<tr><td>Keterangan </td><td>&nbsp; : &nbsp;</td><td><?php echo $row['keterangan_beli']; ?></td></tr>
				</table>
			<?php } ?>
				
            <br/>
			
			<div class="dataTable_wrapper">
				<table class="table table-striped table-bordered ">
					<thead>
						<tr>
							<th><center>No.</center></th>
							<th><center>Kode Produk</center></th>
							<th>Nama Produk</th>
							<th><center>Harga Satuan</center></th>
							<th><center>Jumlah Beli</center></th>
							<th><center>Total Harga</center></th>
						</tr>
					</thead>
					<tbody>
					<?php
					$jumlah_desimal = "0";
					$pemisah_desimal = ",";
					$pemisah_ribuan = ".";
					$total=mysql_fetch_array(mysql_query("SELECT * FROM pembelian where kode_beli='$_GET[kode_beli]'"));
					$sqlDetail = mysql_query("SELECT a.produk_id, a.jumlah, a.harga, a.subtotal, b.nama, b.kode FROM detail_pembelian a, produk b WHERE a.produk_id=b.id AND kode_beli='$_GET[kode_beli]'");
					$no=1;
					while ($row = mysql_fetch_array($sqlDetail)) {
					?>
						<tr>
							<td align="center"><?php echo $no; ?></td>
							<td align="center"><?php echo $row['kode']; ?></font></td>
							<td ><?php echo $row['nama']; ?></td>
							<td align="center"><b><?php echo number_format($row['harga'],$jumlah_desimal,$pemisah_desimal,$pemisah_ribuan).",-"; ?></b></td>
							<td align="center"><?php echo $row['jumlah']; ?></td>
							<td align="right"><b><?php echo number_format($row['subtotal'],$jumlah_desimal,$pemisah_desimal,$pemisah_ribuan).",-"; ?></b></td>
						</tr>
					<?php $no++; } ?>
						<tr>
							<th class="success" colspan='5'><center>Total</center></th>
							<th class="danger" colspan='2'><center><b><?php echo number_format($total['total_beli'],$jumlah_desimal,$pemisah_desimal,$pemisah_ribuan).",-"; ?></b></center></th>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
</div>