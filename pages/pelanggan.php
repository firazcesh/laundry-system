
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Daftar Pelanggan</h1>
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-6">
				<a href="dashboard.php?p=tambah_pelanggan"><button class="btn btn-success"> + Tambah Pelanggan </button></a>
			</div>
			<div class="col-lg-6">
			<?php
				//kode php ini kita gunakan untuk menampilkan pesan eror
				if (!empty($_GET['psn'])) {
					if ($_GET['psn'] == 1) {
						writeMsg('save.sukses');
						} else if ($_GET['psn'] == 2) {
							writeMsg('update.gagal');
							} else if ($_GET['psn'] == 3) {
								writeMsg('update.sukses');
								} else if ($_GET['psn'] == 4) {
									writeMsg('update.gagal');
									} 
					}
			?>
			</div>
		</div>
		
		<br />
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-info">
                    <div class="panel-heading">
                        Daftar Pelanggan
                    </div>

                    <div class="panel-body">
                        <div class="dataTable_wrapper">
							<table class="table table-striped table-bordered table-paginate">
								<thead>
									<tr>
										<th><center>No.</center></th>
										<th><center>Kode Pelanggan</center></th>
										<th>Nama Pelanggan</th>
										<th>Alamat</th>
										<th>No. Tlp</th>
										<th width="15%">Aksi</th>
									</tr>
								</thead>
								<tbody>
								<?php
									$sql = mysql_query("SELECT * FROM pelanggan ORDER BY kode ASC");
									$no=1;
									while ($row = mysql_fetch_array($sql)) {
								?>
									<tr>
										<td align="center"><?php echo $no; ?></td>
										<td align="center"><?php echo $row['kode']; ?></td>
										<td><?php echo $row['nama']; ?></td>
										<td><?php echo $row['alamat']; ?></td>
										<td><?php echo $row['no_tlp']; ?></td>
										<td align="center">
											<a class="btn btn-xs btn-primary" href="dashboard.php?p=edit_pelanggan&id=<?php echo $row['id']; ?>"><span class="glyphicon glyphicon-edit"> Edit</span></a>
										</td>
									</tr>
								<?php $no++; } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>