	<div class="container">
		<h3>Laporan Pembelian</h3>
		<hr>
		
		<div class="row">
			<div class="col-lg-12">
				<form class="form-horizontal" method="GET">	
					<input type="hidden" name="p" id="p"  value="laporan_pembelian"/>
					
					<div class="form-group">
						<label for="awal" class="col-lg-2 control-label">Dari Tanggal :</label>
						<div class="col-lg-2">
							<input type="date" class="form-control" id="awal" name="awal" required>
						</div>
					</div>
						
					<div class="form-group">
						<label for="akhir" class="col-lg-2 control-label">Sampai Tanggal :</label>
						<div class="col-lg-2">
							<input type="date" class="form-control" id="akhir" name="akhir" required>
						</div>
					</div>
						
					<div class="form-actions">
						<div class="col-lg-12">
							<button class="btn btn-success" type="submit" value="Search">Lihat</button>
						</div>
					</div>
				</form>
				
				<?php
					if (isset($_GET['awal'])) {
						$tgl_a = $_GET['awal'];
						$tgl_b = $_GET['akhir'];
							echo "<br /><h2 align='center'>Pembelian Periode <b>$tgl_a</b> Sampai <b>$tgl_b</b></h2><br />";
								echo "<div class='form-group form-horizontal'>
										<div class='col-lg-3 col-lg-offset-9' align='right'>
											<a href='dashboard.php?p=excel_pembelian&awal=$_GET[awal]&akhir=$_GET[akhir]'>
											<button class='btn btn-success'> Export to Excel </button></a>&nbsp;
											<a href='dashboard.php?p=pdf_lap_pembelian&awal=$_GET[awal]&akhir=$_GET[akhir]'>
											<button class='btn btn-success'> Export to PDF </button></a>
										</div>
									</div>";
								echo "<table class='table table-bordered table-hover'>";
								echo "<thead>
									<tr>
										<th width='5%'><center>No.</center></th>
										<th width='10%'><center>Tanggal</center></th>
										<th width='10%'><center>Kode Beli</center></th>
										<th width='15%'><center>Nomor Nota</center></th>
										<th width='30%'><center>Suplier</center></th>
										<th width='20%'><center>Pegawai</center></th>
										<th width='10%'><center>Total</center></th>
									</tr>
								</thead>";
								$no=1;
								$total=mysql_fetch_array(mysql_query("select sum(total_beli) as ttl FROM pembelian WHERE tanggal_beli BETWEEN '$tgl_a' AND '$tgl_b'"));
										
								$sql = mysql_query("SELECT a.tanggal_beli, a.kode_beli, a.nota_beli, a.total_beli, a.pegawai, b.nama as suplier FROM pembelian a, suplier b WHERE a.suplier=b.id AND tanggal_beli BETWEEN '$tgl_a' AND '$tgl_b' ORDER BY tanggal_beli ASC");
								$cek = mysql_num_rows($sql);
									if ($cek >= 1) {
										while ($row = mysql_fetch_array($sql)) {
								echo "<tbody>
										<tr>
											<td align='center'>$no</td>
											<td align='center'>$row[tanggal_beli]</td>
											<td align='center'><font color='blue'><a href='dashboard.php?p=lihat_pembelian&kode_beli=$row[kode_beli]'>$row[kode_beli]</a></font></td>
											<td align='center'>$row[nota_beli]</td>
											<td>$row[suplier]</td>
											<td>$row[pegawai]</td>
											<td align='right'><b>$row[total_beli]</b></td>
										</tr>";
									$no++; }
									echo"<tr>
											<td class='success' align='center' colspan='6'><b>Total<b></td>
											<td class='danger' align='center' colspan='1'><b>$total[ttl]</b></td>								
										</tr>";
										}
											else{
									echo"<tr>
											<td colspan='7'><center><b>TIDAK ADA TRANSAKSI</b></center></td>
										</tr>";
										}
								echo "</tbody>";
							echo "</table>";
						}
					?>
			</div>
		</div>