	<?php
	//Kode ini untuk mengurutkan otomatis kode suplier
		$sql=mysql_query("SELECT kode from suplier order by kode DESC LIMIT 0,1");
		$data=mysql_fetch_array($sql);
		$kodeawal=substr($data['kode'],2,3)+1;
			if($kodeawal<10){
				$kode='SP00'.$kodeawal;
				}
				elseif($kodeawal > 9 && $kodeawal <=99){
					$kode='SP0'.$kodeawal;
				}else{
					$kode='SP0'.$kodeawal;
				}
	?>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Tambah Suplier</h1>
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-4">
				<a href="dashboard.php?p=suplier"><button class="btn btn-danger"><span class="glyphicon glyphicon-arrow-left"> Kembali</span></button></a>
			</div>
			<div class="col-lg-4">
				<span id="head" class="label label-success"></span>
			</div>
		</div>
		<br />
		

		<div class="row">
			<div class="col-lg-12">
				<form class="form-horizontal" method="POST" onsubmit='return formValidation()'>
					<div class="form-group">
						<label for="kode" class="col-lg-2 control-label">Kode Suplier :</label>
						<div class="col-lg-2">
							<input type="text" maxlength="5" class="form-control" id="kode" name="kode" value="<?php echo $kode;?>" readonly>
						</div>
					</div>
					
					<div class="form-group">
						<label for="nama" class="col-lg-2 control-label">Nama Suplier :</label>
						<div class="col-lg-6">
							<input type="text" maxlength="50" class="form-control" id="nama" name="nama" placeholder="Nama suplier ...">
						</div>
						<span id="nm1" class="label label-success"></span><span id="nm2" class="label label-warning"></span>
					</div>
					
					<div class="form-group">
						<label for="alamat" class="col-lg-2 control-label">Alamat :</label>
						<div class="col-lg-6">
							<textarea  maxlength="255" class="form-control" id="alamat" name="alamat" required></textarea>
						</div>
					</div>
					
					<div class="form-group">
						<label for="no_tlp" class="col-lg-2 control-label">No. Telepon :</label>
						<div class="col-lg-6">
							<input type="text" maxlength="12" class="form-control" id="no_tlp" name="no_tlp" placeholder="Nomor telepon ..." >
						</div>
						<span id="nt1" class="label label-success"></span><span id="nt2" class="label label-warning"></span>
					</div>
					
					<div class="form-group">
						<label for="email" class="col-lg-2 control-label">Email :</label>
						<div class="col-lg-6">
							<input type="text"  maxlength="100" class="form-control" id="email" name="email" placeholder="Email ...">
						</div>
						<span id="em1" class="label label-success"></span><span id="em2" class="label label-warning"></span>
					</div>
					
					<div class="form-group">
						<div class="col-lg-10 col-lg-offset-2">
							<button class="btn btn-danger" type="reset" value="Reset" >Reset</button>
							<button class="btn btn-success" type="submit" value="Simpan" name="simpan">Simpan</button>
						</div>
					</div>
					<?php
						if (isset($_POST["simpan"])) {
							$query="INSERT INTO suplier (kode, nama, alamat, no_tlp, email) VALUES ('$_POST[kode]','$_POST[nama]','$_POST[alamat]', '$_POST[no_tlp]', '$_POST[email]')";
							
							$sql = mysql_query($query); 
								
							if ($sql){
								header('Location: dashboard.php?p=suplier&psn=2');
									}else{
										header('Location: dashboard.php?p=suplier&psn=3');
									}					
						}
					?>
				</form>
			</div>
		</div>
	</div>
	<script language="JavaScript" type="text/javascript">
	function formValidation(){

		// Make quick references to our fields	
		
		var nama =  document.getElementById('nama');
		var email =  document.getElementById('email');
		var no_tlp =  document.getElementById('no_tlp');

		//  to check empty form fields.

		if(nama.value.length == 0){
			document.getElementById('head').innerText = "Semua form harus diisi!"; //this segment displays the validation rule for all fields
			nama.focus();
			return false;
		} 
		
		if(textAlphanumericspace(nama, "Isi form tanpa karakter spesial")){
			
			if(lengthDefinenm(nama, 5, 100)){
				
				if(textNumericst(no_tlp, "Isi form dengan angka!")){
							
					if(lengthDefinent(no_tlp, 7, 12)){
						
						if(emailValidation(email, "Format email salah ..!")){
					
						return true;
						}
					}
				}
			}
		}
		
		return false;
		
	}
	
	//Nama
	//function that checks whether input text includes alphabetic and numeric characters.
	function textAlphanumericspace(inputtext, alertMsg){
		var alphaExp = /^[0-9a-zA-Z .()]+$/;
		if(inputtext.value.match(alphaExp)){
			return true;
		}else{
			document.getElementById('nm1').innerText = alertMsg; //this segment displays the validation rule for address
			inputtext.focus();
			return false;
		}
	}
	function lengthDefinenm(inputtext, min, max){
		var uInput = inputtext.value;
		if(uInput.length >= min && uInput.length <= max){
			return true;
		}else{
			
			document.getElementById('nm2').innerText = "* Masukkan " +min+ " sampai " +max+ " karakter *"; 
			inputtext.focus();
			return false;
		}
	}

	
	//No. Tlp
	function textNumericst(inputtext, alertMsg){
		var numericExpression = /^[0-9]+$/;
		if(inputtext.value.match(numericExpression)){
			return true;
		}else{
			document.getElementById('nt1').innerText = alertMsg;  //this segment displays the validation rule for zip
			inputtext.focus();
			return false;
		}
	}
	// Function that checks whether the input characters are restricted according to defined by user.
	function lengthDefinent(inputtext, min, max){
		var uInput = inputtext.value;
		if(uInput.length >= min && uInput.length <= max){
			return true;
		}else{
			
			document.getElementById('nt2').innerText = "* Masukkan " +min+ " sampai " +max+ " karakter *"; 
			inputtext.focus();
			return false;
		}
	}
	
	function emailValidation(inputtext, alertMsg){
		var emailExp = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
		if(inputtext.value.match(emailExp)){
			return true;
		}else{
			document.getElementById('em1').innerText = alertMsg; //this segment displays the validation rule for email
			inputtext.focus();
			return false;
		}
	}
	</script>