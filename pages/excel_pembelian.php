<!DOCTYPE html>
	<?php
	header("Content-Type: application/force-download");
	header("Cache-Control: no-cache, must-revalidate");
	header("Expires: Sat, 26 Jul 2010 05:00:00 GMT");
	header("content-disposition: attachment;filename=laporan_pembelian".date('dmY').".xls");
	$tgl_a = $_GET['awal'];
	$tgl_b = $_GET['akhir'];
	?>
	<h1 align="center">Laporan Pembelian</h1>
	<h5 align="center">Periode <?php echo $tgl_a ?> s/d <?php echo $tgl_b; ?></h5>
	<hr>
	<br />
	<h2 align="center">Pembelian Periode <?php echo $tgl_a ?> s/d <?php echo $tgl_b; ?></h2>
	<table align="center" border="1">
		<tr>
			<th><center>NO</th>
			<th><center>Tanggal</th>
			<th><center>Kode Beli</th>
			<th><center>Nota</th>
			<th><center>Suplier</th>
			<th><center>Pegawai</th>
			<th><center>Total Transaksi</th>
		</tr>
		<?php
			$jumlah_desimal = "0";
			$pemisah_desimal = ",";
			$pemisah_ribuan = ".";
			$nomor = 1;
			$tot=mysql_fetch_array(mysql_query("SELECT SUM(subtotal) as tt, a.kode_beli, b.tanggal_beli, b.kode_beli FROM detail_pembelian a, pembelian b WHERE a.kode_beli = b.kode_beli AND b.tanggal_beli BETWEEN '$tgl_a' AND '$tgl_b'"));
			$total=mysql_fetch_array(mysql_query("select sum(total_beli) as ttl FROM pembelian WHERE tanggal_beli BETWEEN '$tgl_a' AND '$tgl_b'"));
			$sql = mysql_query("SELECT a.tanggal_beli, a.kode_beli, a.nota_beli, a.total_beli, a.pegawai, b.nama as suplier FROM pembelian a, suplier b WHERE a.suplier=b.id AND tanggal_beli BETWEEN '$tgl_a' AND '$tgl_b' ORDER BY tanggal_beli");
			while ($row = mysql_fetch_array($sql)) {
		?>
		<tr>
			<td align="center"><?php echo $nomor++; ?></td>
			<td align="center"><?php echo $row['tanggal_beli']; ?></td>
			<td align="center"><?php echo $row['kode_beli']; ?></td>
			<td align="center"><?php echo $row['nota_beli'] ?></td>
			<td><?php echo $row['suplier']; ?></td>
			<td><?php echo $row['pegawai']; ?></td>
			<td align="right"><b><?php echo number_format($row['total_beli'],$jumlah_desimal,$pemisah_desimal,$pemisah_ribuan).",-"; ?></b></td>
		</tr>
		<?php
			}
		?>
		<tr>
			<td class='table-td' align="center;" colspan='6'><b>Total</b></td>
			<td class='table-td' align="center;" colspan='1'><b><?php echo number_format($total['ttl'],$jumlah_desimal,$pemisah_desimal,$pemisah_ribuan).",-"; ?></b></td>								
		</tr>
	</table>
	<br />
	<br />
	<hr>
	<h3 align="center">Detail Pembelian Periode <?php echo $tgl_a ?> s/d <?php echo $tgl_b; ?></h3>
	<table align="center" border="1">
		<tr>
			<th><center>NO</th>
			<th><center>Kode Invoice</th>
			<th><center>Kode Produk</th>
			<th><center>Nama Produk</th>
			<th><center>Jumlah</th>
			<th><center>Harga</th>
			<th><center>Subtotal</th>
		</tr>
		<?php
			$jumlah_desimal = "0";
			$pemisah_desimal = ",";
			$pemisah_ribuan = ".";
			$no = 1;
			$sql = mysql_query("SELECT a.tanggal_beli, a.kode_beli, b.kode_beli, b.produk_id, b.jumlah, b.harga, b.subtotal, c.nama, c.kode
								FROM pembelian a, detail_pembelian b, produk c
								WHERE a.kode_beli = b.kode_beli AND b.produk_id=c.id AND a.tanggal_beli BETWEEN '$tgl_a' AND '$tgl_b'
								ORDER BY a.kode_beli");
			while ($r = mysql_fetch_array($sql)) {
		?>
		<tr>
			<td align="center"><?php echo $no++; ?></td>
			<td align="center"><?php echo $r['kode_beli']; ?></td>
			<td align="center"><?php echo $r['kode'] ?></td>
			<td><?php echo $r['nama']; ?></td>
			<td align="center"><?php echo $r['jumlah']; ?></td>
			<td align="right"><b><?php echo number_format($r['harga'],$jumlah_desimal,$pemisah_desimal,$pemisah_ribuan).",-"; ?></b></td>
			<td align="right"><b><?php echo number_format($r['subtotal'],$jumlah_desimal,$pemisah_desimal,$pemisah_ribuan).",-"; ?></b></td>
		</tr>
		<?php
			}
		?>
		<tr>
			<td class='table-td' align="center;" colspan='6'><b>Total</b></td>
			<td class='table-td' align="center;" colspan='1'><b><?php echo number_format($tot['tt'],$jumlah_desimal,$pemisah_desimal,$pemisah_ribuan).",-"; ?></b></td>								
		</tr>
	</table>
</html>